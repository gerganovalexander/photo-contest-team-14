package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.ContestMapper;
import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.ContestCreateDto;
import com.company.web.photo_contest.modelsDto.ContestResponseDto;
import com.company.web.photo_contest.repositories.CategoryRepository;
import com.company.web.photo_contest.repositories.ContestRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import com.company.web.photo_contest.services.contracts.JurorService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;

import java.time.LocalDateTime;
import java.util.*;

import static com.company.web.photo_contest.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository mockContestRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    CategoryRepository categoryRepository;
    @Mock
    ContestMapper mockContestMapper;
    @Mock
    JurorService jurorService;
    @InjectMocks
    ContestServiceImpl mockContestService;

    @Test
    void getAll_Should_CallRepository() {
        //Arrange
        when(mockContestRepository.findAll())
                .thenReturn(null);

        //Act
        mockContestService.getAll();

        //Assert
        verify(mockContestRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getContestByUuid_Should_ReturnContest_When_ContestWithUuidExists() {
        //Arrange
        Contest mockContest = createMockContest();

        when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));

        //Act
        Contest result = mockContestService.getContestByUuid(createMockContest().getUuid());

        //Assert
        assertEquals(result, mockContest);
    }

    @Test
    public void getContestByUuid_Should_Throw_When_ContestDoseNotExists() {
        //Arrange
        Contest mockContest = createMockContest();

        when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.empty());

        // Act, Assert
        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> mockContestService.getContestByUuid(mockContest.getUuid()));
    }

    @Test
    public void getContestsByTitle_Should_CallRepository_When_ContestWhitTitleExists() {
        //Arrange
        Contest mockContest = createMockContest();

        when(mockContestRepository.getContestsByTitle(mockContest.getTitle()))
                .thenReturn(Optional.of(mockContest));

        //Act
        Contest result = mockContestService.getContestsByTitle(createMockContest().getTitle());

        //Assert
        assertEquals(result, mockContest);
    }

    @Test
    public void getContestsByTitle_Should_Throw_When_ContestWhitTitleDoseNotExists() {
        //Arrange
        Contest mockContest = createMockContest();

        when(mockContestRepository.getContestsByTitle(mockContest.getTitle()))
                .thenReturn(Optional.empty());

        // Act, Assert
        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> mockContestService.getContestsByTitle(mockContest.getTitle()));
    }

    @Test
    public void mapContest_Should_ConvertContestToContestResponseDto() {
        //Arrange
        Contest mockContest = createMockContest();
        ContestResponseDto mockContestDto = createMockContestDto();

        when(mockContestRepository.getContestByUuid(mockContest.getUuid()))
                .thenReturn(Optional.of(mockContest));
        when(mockContestMapper.viewContest(mockContest))
                .thenReturn(mockContestDto);

        //Act
        ContestResponseDto result = mockContestService.mapContest(mockContest.getUuid());

        //Assert
        assertEquals(result, mockContestDto);
    }

    @Test
    public void findContestByTitle_Should_CallRepository_WhenTitleIsPresent() {
        List<Contest> mockContests = new ArrayList<>();
        mockContests.add(createMockContest());
        mockContests.add(createMockContest());

        when(mockContestRepository.findContestByTitle("title"))
                .thenReturn(Optional.of(mockContests));

        List<Contest> contests = mockContestService.findContestByTitle("title");

        verify(mockContestRepository, times(2)).findContestByTitle("title");
        assertEquals(mockContests, contests);
    }

    @Test
    public void findContestByTitle_WhenTitleDoseNotExists() {
        // Arrange
        Mockito.when(mockContestRepository.findContestByTitle("nonexistent title"))
                .thenReturn(Optional.empty());

        // Act
        List<Contest> contests = mockContestService.findContestByTitle("nonexistent title");

        // Assert
        assertEquals(0, contests.size());
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .findContestByTitle("nonexistent title");
    }

    @Test
    public void findContestByCategory_Should_CallRepository() {
        Category categoryName = createMockCategory();

        List<Contest> expectedContests = new ArrayList<>();
        expectedContests.add(createMockContest());
        expectedContests.add(createMockContest());

        when(mockContestRepository.findContestByCategoryName(categoryName.getName()))
                .thenReturn(Optional.of(expectedContests));

        List<Contest> actualContests = mockContestService
                .findContestByCategory(categoryName.getName());

        verify(mockContestRepository, times(2))
                .findContestByCategoryName(categoryName.getName());
        assertEquals(expectedContests, actualContests);
    }

    @Test
    public void findContestByCategory_WithNonExistentCategory() {
        // Arrange
        String nonExistentCategory = "non-existent category";
        when(mockContestRepository.findContestByCategoryName(nonExistentCategory))
                .thenReturn(Optional.empty());

        // Act
        List<Contest> result = mockContestService
                .findContestByCategory(nonExistentCategory);

        // Assert
        assertTrue(result.isEmpty());
        verify(mockContestRepository, times(1))
                .findContestByCategoryName(nonExistentCategory);
    }

    @Test
    public void findIsFinished_CallRepository() {
        List<Contest> finishedContests = new ArrayList<>();
        finishedContests.add(createMockContest());
        finishedContests.add(createMockContest());

        Contest contest1 = createMockContest();
        contest1.setFinished(false);
        Contest contest2 = createMockContest();
        contest1.setFinished(false);

        List<Contest> unfinishedContests = new ArrayList<>();
        unfinishedContests.add(contest1);
        unfinishedContests.add(contest2);

        when(mockContestRepository.findContestByIsFinished(true))
                .thenReturn(Optional.of(finishedContests));
        when(mockContestRepository.findContestByIsFinished(false))
                .thenReturn(Optional.of(unfinishedContests));

        List<Contest> foundFinishedContests = mockContestService.findIsFinished(true);
        List<Contest> foundUnfinishedContests = mockContestService.findIsFinished(false);

        assertEquals(finishedContests, foundFinishedContests);
        assertEquals(unfinishedContests, foundUnfinishedContests);
    }

    @Test
    public void findIsFinished_WhenNonExistent() {
        // Arrange
        boolean isFinished = true;
        when(mockContestRepository.findContestByIsFinished(isFinished))
                .thenReturn(Optional.empty());

        // Act
        List<Contest> result = mockContestService.findIsFinished(isFinished);

        // Assert
        verify(mockContestRepository).findContestByIsFinished(isFinished);
        assertTrue(result.isEmpty());
    }

    @Test
    public void getActiveContests_Should_CallRepository() {
        List<Contest> mockContests = new ArrayList<>();
        Contest contest1 = createMockContest();
        contest1.setPhaseOne(LocalDateTime.now().minusDays(2));
        Contest contest2 = createMockContest();
        contest2.setPhaseOne(LocalDateTime.now().plusDays(1));
        mockContests.add(contest1);
        mockContests.add(contest2);

        Mockito.when(mockContestRepository.findByPhaseOneAfter(Mockito.any(LocalDateTime.class)))
                .thenReturn(mockContests);

        List<Contest> activeContests = mockContestService.getActiveContests();

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .findByPhaseOneAfter(Mockito.any(LocalDateTime.class));

        assertEquals(1, activeContests.size());
        assertEquals(contest2, activeContests.get(0));
    }

    @Test
    public void isFinishedCount_Should_CallRepository() {
        Mockito.when(mockContestRepository.countByIsFinishedTrue())
                .thenReturn(2);

        int count = mockContestService.isFinishedCount();

        assertEquals(2, count);

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .countByIsFinishedTrue();
    }

    @Test
    public void findContestByPhase_Should_CallRepository() {
        LocalDateTime phase = LocalDateTime.now();
        List<Contest> expectedContests = Arrays.asList(createMockContest(), createMockContest());

        when(mockContestRepository.findContestByPhaseOne(phase))
                .thenReturn(Optional.of(expectedContests));

        List<Contest> actualContests = mockContestService.findContestByPhase(phase);

        assertEquals(expectedContests, actualContests);
        verify(mockContestRepository, times(2))
                .findContestByPhaseOne(phase);
    }

    @Test
    public void findContestByPhase_WhenNoContestsExist() {
        List<Contest> emptyList = new ArrayList<>();

        Mockito.when(mockContestRepository.findContestByPhaseOne(Mockito.any(LocalDateTime.class)))
                .thenReturn(Optional.of(emptyList));

        List<Contest> contests = mockContestService.findContestByPhase(LocalDateTime.now());

        assertTrue(contests.isEmpty());

        Mockito.verify(mockContestRepository, Mockito.times(2))
                .findContestByPhaseOne(Mockito.any(LocalDateTime.class));
    }

    @Test
    public void findContestByPhase_Should_ReturnsEmptyList() {
        // Arrange
        LocalDateTime phase = LocalDateTime.of(2023, 4, 24, 10, 0);
        List<Contest> emptyList = new ArrayList<>();

        Mockito.when(mockContestRepository.findContestByPhaseOne(phase))
                .thenReturn(Optional.empty());

        // Act
        List<Contest> result = mockContestService.findContestByPhase(phase);

        // Assert
        assertEquals(result, emptyList);
    }

    @Test
    public void findContestByPhaseTwo_Should_CallRepository() {
        LocalDateTime phase = LocalDateTime.now();
        List<Contest> expectedContests = Arrays.asList(createMockContest(), createMockContest());

        Mockito.when(mockContestRepository.findContestByPhaseTwo(phase))
                .thenReturn(Optional.of(expectedContests));

        List<Contest> actualContests = mockContestService.findContestByPhaseTwo(phase);

        assertEquals(expectedContests, actualContests);
        verify(mockContestRepository, times(2))
                .findContestByPhaseTwo(phase);
    }

    @Test
    public void findContestByPhaseTwo_Should_CallRepository_WhenNoContestsExist() {
        List<Contest> emptyList = new ArrayList<>();

        Mockito.when(mockContestRepository.findContestByPhaseTwo(Mockito.any(LocalDateTime.class)))
                .thenReturn(Optional.of(emptyList));

        List<Contest> contests = mockContestService
                .findContestByPhaseTwo(LocalDateTime.now());

        assertTrue(contests.isEmpty());

        Mockito.verify(mockContestRepository, Mockito.times(2))
                .findContestByPhaseTwo(Mockito.any(LocalDateTime.class));
    }

    @Test
    public void findContestByPhaseTwo_Should_ReturnsEmptyList() {
        // Arrange
        LocalDateTime phase = LocalDateTime.of(2023, 4, 24, 10, 0);
        List<Contest> emptyList = new ArrayList<>();
        given(mockContestRepository.findContestByPhaseTwo(phase))
                .willReturn(Optional.empty());

        // Act
        List<Contest> result = mockContestService.findContestByPhaseTwo(phase);

        // Assert
        assertEquals(result, emptyList);
    }

    @Test
    void findByFilter_Should_CallRepository() {
        Category category = createMockCategory();

        Contest contest1 = createMockContest();
        Contest contest2 = createMockContest();

        List<Contest> expected = new ArrayList<>();
        expected.add(contest1);
        expected.add(contest2);

        when(mockContestRepository.filterByFilters("title", category, true))
                .thenReturn(expected);

        List<Contest> actual = mockContestService.findByFilter("title", category, true);

        assertEquals(expected, actual);

        verify(mockContestRepository, times(1))
                .filterByFilters("title", category, true);
    }

    @Test
    void filter_Should_CallRepository() {
        Contest contest = createMockContest();
        Category category = createMockCategory();
        Boolean isFinished = false;

        when(mockContestRepository.filter(contest.getTitle(), category, isFinished))
                .thenReturn(Arrays.asList(new Contest(), new Contest()));

        List<Contest> result = mockContestService
                .filter(contest.getTitle(), category, isFinished);

        Mockito.verify(mockContestRepository, times(1))
                .filter(contest.getTitle(), category, isFinished);
        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    void getAllContestsWhereUserParticipatesIn() {
        UUID userUuid = UUID.randomUUID();
        Pageable pageable = PageRequest.of(0, 6);

        when(userRepository.findByUuid(userUuid))
                .thenReturn(Optional.of(createMockUser()));

        mockContestService.getAllContestWhereUserParticipatesIn(userUuid, 0, 6);

        Mockito.verify(mockContestRepository, times(1))
                .findAllContestsUserParticipatesIn(createMockUser().getUserId(), pageable);
    }

    @Test
    void isCompleted_Should_ReturnsEmptyList_WhenNoContestsCompleted() {
        LocalDateTime now = LocalDateTime.now();
        Contest inProgressContest = new Contest();

        inProgressContest.setPhaseTwo(now.plusDays(1));
        mockContestRepository.save(inProgressContest);

        List<Contest> completedContests = mockContestService.isCompleted(now);
        assertTrue(completedContests.isEmpty());
    }

    @Test
    void isCompleted_Should_ReturnsEmptyList_WhenNoContestsExist() {
        List<Contest> completedContests = mockContestService
                .isCompleted(LocalDateTime.now());

        assertTrue(completedContests.isEmpty());
    }

    @Test
    public void isCompleted() {
        Contest contest = createMockContest();

        List<Contest> completedContests = new ArrayList<>();
        completedContests.add(contest);

        when(mockContestRepository.isCompleted(any()))
                .thenReturn(completedContests);

        List<Contest> result = mockContestService.isCompleted(LocalDateTime.now());

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(contest.getUuid(), result.get(0).getUuid());
    }

    @Test
    public void createContest_Should_CreateContest() {
        User user = createMockUser();

        Category category = createMockCategory();
        categoryRepository.save(category);

        ContestCreateDto dto = new ContestCreateDto();
        dto.setTitle("Contest Title");
        dto.setCategoryName(category.getName());
        dto.setUser(new ArrayList<>());

        Mockito.when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockContestMapper.fromDto(dto, user.getUsername()))
                .thenReturn(createMockContest());

        Mockito.when(categoryRepository.getCategoryByName(dto.getCategoryName()))
                .thenReturn(Optional.of(category));

        mockContestService.createContest(dto, user.getUsername());

        Mockito.verify(mockContestRepository, Mockito.times(1))
                .save(Mockito.any(Contest.class));
    }

    @Test
    void createContest_Should_Throw_WhenContestExists() {
        ContestCreateDto contestCreateDto = new ContestCreateDto();
        contestCreateDto.setTitle("Test Contest");

        contestCreateDto.setCategoryName("Test Category");

        User user = createMockUser();

        Category category = createMockCategory();
        categoryRepository.save(category);

        Contest existingContest = createMockContest();
        existingContest.setTitle("Test Contest");

        mockContestRepository.save(existingContest);

        Mockito.when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockContestRepository.getContestsByTitle("Test Contest"))
                .thenReturn(Optional.of(existingContest));

        assertThrows(EntityExistsException.class,
                () -> mockContestService.createContest(contestCreateDto, user.getUsername()));

        Mockito.verify(jurorService, Mockito.times(0))
                .create(Mockito.any(UUID.class), Mockito.any(UUID.class));
    }

    @Test
    void createContest_Should_Throws_EntityNotFoundException() {
        ContestCreateDto dto = new ContestCreateDto();
        dto.setTitle("New Contest");
        dto.setCategoryName("Non-existent Category");

        assertThrows(EntityNotFoundException.class,
                () -> mockContestService.createContest(dto, "test_user"));
    }

    @Test
    void createContest_Should_Throw_WithExistingTitle() {
        // Arrange
        String username = "test_user";
        String title = "Existing Contest";

        ContestCreateDto dto = new ContestCreateDto();
        dto.setTitle(title);

        User user = new User();
        user.setUsername(username);

        when(userRepository.findByUsername(username))
                .thenReturn(Optional.of(user));
        when(mockContestRepository.getContestsByTitle(title))
                .thenReturn(Optional.of(new Contest()));

        // Act & Assert
        assertThrows(EntityExistsException.class,
                () -> mockContestService.createContest(dto, username));
    }

    @Test
    public void createContest_withUser_createsJurors() {
        // Arrange
        User user = createMockUser();
        ContestCreateDto dto = new ContestCreateDto();
        List<User> jurors = Arrays.asList(createMockUser(), createMockUser());
        dto.setUser(jurors);
        Category category = createMockCategory();
        Contest contest = createMockContest();
        contest.setUuid(UUID.randomUUID());
        contest.setCreatedBy(user);
        contest.setCategory(category);

        Mockito.when(userRepository.findByUsername(user.getUsername()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockContestMapper.fromDto(Mockito.any(ContestCreateDto.class), Mockito.anyString()))
                .thenReturn(contest);

        Mockito.when(categoryRepository.getCategoryByName(dto.getCategoryName()))
                .thenReturn(Optional.of(category));

        // Act
        mockContestService.createContest(dto, user.getUsername());

        // Assert
        jurors.forEach(juror -> Mockito.verify(jurorService, Mockito.times(2))
                .create(contest.getContestId(), juror.getUserId()));
    }

    @Test
    public void updateContest_Should_Throw_WithNonExistingUser() {
        ContestCreateDto dto = new ContestCreateDto();
        dto.setTitle("New Title");

        assertThrows(EntityNotFoundException.class, () -> mockContestService
                .updateContest(UUID.randomUUID(), dto, "nonExistingUser"));
    }

    @Test
    void updateContest_Should_Throw_EntityNotFoundException() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        ContestCreateDto dto = new ContestCreateDto();
        String username = "non-existent-user";

        // Act and Assert
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class,
                () -> mockContestService.updateContest(uuid, dto, username));
        assertEquals("User not found.", exception.getMessage());
    }

    @Test
    public void updateContest_SuccessfulUpdate() {
        UUID contestUuid = UUID.randomUUID();
        ContestCreateDto contestCreateDto = new ContestCreateDto();

        User currentUser = createMockUser();

        Contest contest = createMockContest();
        contest.setCreatedBy(currentUser);

        when(mockContestRepository.getContestByUuid(contestUuid))
                .thenReturn(Optional.of(contest));
        when(userRepository.findByUsername(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));
        when(mockContestMapper.fromDto(contestCreateDto, currentUser.getUsername()))
                .thenReturn(contest);
        when(mockContestMapper.viewContest(contest))
                .thenReturn(new ContestResponseDto());

        ContestResponseDto updatedContest = mockContestService.updateContest(contestUuid, contestCreateDto, currentUser.getUsername());

        assertNotNull(updatedContest);
    }

    @Test
    void updateContest_Should_Throw_WithNonExistingUUID() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        String username = "creator_user";

        ContestCreateDto dto = new ContestCreateDto();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(new User()));
        when(mockContestRepository.getContestByUuid(uuid)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class,
                () -> mockContestService.updateContest(uuid, dto, username));
    }

    @Test
    void updateContest_Should_Throw_WhenUserIsNotCreator() {
        // Arrange
        UUID uuid = UUID.randomUUID();
        User foreignUser = createMockUser();
        foreignUser.setUsername("creator_user");
        ContestCreateDto dto = new ContestCreateDto();

        User creatorUser = createMockUser();
        creatorUser.setUsername("test");
        Contest contest = createMockContest();
        contest.setCreatedBy(creatorUser);

        when(mockContestRepository.getContestByUuid(uuid)).thenReturn(Optional.of(contest));
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(foreignUser));

        // Act & Assert
        assertThrows(AccessDeniedException.class,
                () -> mockContestService.updateContest(uuid, dto, foreignUser.getUsername()));
    }

    @Test
    public void deleteContest_Should_CallRepository() {
        // Arrange
        Contest contest = createMockContest();

        // Act
        mockContestService.deleteContest(contest);

        // Assert
        verify(mockContestRepository, times(1))
                .deleteByUuid(contest.getUuid());
    }

    @Test
    void deleteContest_Should_Throw() {
        UUID uuid = UUID.randomUUID();
        Contest contest = new Contest();
        contest.setUuid(uuid);

        Mockito.doNothing()
                .when(mockContestRepository).deleteByUuid(uuid);

        assertDoesNotThrow(() -> mockContestService.deleteContest(contest));
    }

    @Test
    public void checkContest_Should_CallRepository() {
        Mockito.when(mockContestRepository.isCompleted(Mockito.any(LocalDateTime.class)))
                .thenReturn(Arrays.asList(new Contest(), new Contest()));

        mockContestService.checkContest();

        Mockito.verify(mockContestRepository, Mockito.times(2))
                .finished(Mockito.any(UUID.class));
    }

    @Test
    public void changeIsGraded() {
        UUID contestUuid = UUID.randomUUID();
        mockContestService.changeIsGraded(contestUuid);
        verify(mockContestRepository).graded(contestUuid);
    }

    @Test
    public void getCompletedContestsAsPage_Should_ReturnsEmptyPage() {
        LocalDateTime completed = LocalDateTime.now();

        Pageable pageable = PageRequest.of(0, 10);
        Page<Contest> emptyPage = Page.empty();

        Mockito.when(mockContestRepository.findContestsByIsCompleted(completed, pageable))
                .thenReturn(emptyPage);

        Page<Contest> page = mockContestService.getCompletedContestsAsPage(completed, 0, 10);

        assertTrue(page.isEmpty());
    }

    @Test
    public void getContestsInPhaseTwoAsPage_Should_ReturnsEmptyPage() {
        LocalDateTime phaseTwo = LocalDateTime.now();
        Pageable pageable = PageRequest.of(0, 10);
        Page<Contest> emptyPage = Page.empty();

        Mockito.when(mockContestRepository.findContestsByDatePhaseTwo(phaseTwo, pageable))
                .thenReturn(emptyPage);

        Page<Contest> page = mockContestService.getContestsInPhaseTwoAsPage(phaseTwo, 0, 10);

        assertTrue(page.isEmpty());
    }

    @Test
    public void getContestsBetweenPhaseOneAsPage_Should_returnsEmptyPage() {
        LocalDateTime phaseOne = LocalDateTime.now();
        Pageable pageable = PageRequest.of(0, 10);
        Page<Contest> emptyPage = Page.empty();

        Mockito.when(mockContestRepository.findContestByDateBetweenCreateAndPhaseOne(phaseOne, pageable))
                .thenReturn(emptyPage);

        Page<Contest> page = mockContestService.getContestsBetweenPhaseOneAsPage(phaseOne, 0, 10);

        assertTrue(page.isEmpty());
    }

    @Test
    public void getContestsInPhaseTwoAsPage_Should_ReturnsNonEmptyPage() {
        LocalDateTime phaseTwo = LocalDateTime.now();
        Pageable pageable = PageRequest.of(0, 10);
        List<Contest> contestList = new ArrayList<>();

        Contest contest1 = createMockContest();
        Contest contest2 = createMockContest();
        contestList.add(contest1);
        contestList.add(contest2);

        Page<Contest> page = new PageImpl<>(contestList, pageable, contestList.size());

        Mockito.when(mockContestRepository.findContestsByDatePhaseTwo(phaseTwo, pageable))
                .thenReturn(page);

        Page<Contest> resultPage = mockContestService.getContestsInPhaseTwoAsPage(phaseTwo, 0, 10);

        assertFalse(resultPage.isEmpty());
        assertEquals(page.getTotalElements(), resultPage.getTotalElements());
        assertEquals(page.getContent().get(0), resultPage.getContent().get(0));
        assertEquals(page.getContent().get(1), resultPage.getContent().get(1));
    }

    @Test
    public void getContestsBetweenPhaseOneAsPage_Should_ReturnsNonEmptyPage() {
        LocalDateTime phaseOne = LocalDateTime.now();
        Pageable pageable = PageRequest.of(0, 10);
        List<Contest> contestList = new ArrayList<>();

        Contest contest1 = createMockContest();
        Contest contest2 = createMockContest();
        contestList.add(contest1);
        contestList.add(contest2);
        Page<Contest> page = new PageImpl<>(contestList, pageable, contestList.size());

        Mockito.when(mockContestRepository.findContestByDateBetweenCreateAndPhaseOne(phaseOne, pageable))
                .thenReturn(page);

        Page<Contest> resultPage = mockContestService.getContestsBetweenPhaseOneAsPage(phaseOne, 0, 10);

        assertFalse(resultPage.isEmpty());
        assertEquals(page.getTotalElements(), resultPage.getTotalElements());
        assertEquals(page.getContent().get(0), resultPage.getContent().get(0));
        assertEquals(page.getContent().get(1), resultPage.getContent().get(1));
    }
}
