package com.company.web.photo_contest;

import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.*;

import java.time.LocalDateTime;
import java.util.UUID;

public class Helpers {

    public static Participation createMockParticipation() {
        var mockParticipation = new Participation();
        mockParticipation.setParticipationId(1);
        mockParticipation.setUser(createMockUser());
        mockParticipation.setContest(createMockContest());
        mockParticipation.setDateCreated(LocalDateTime.now());
        mockParticipation.setPhoto("photo");
        mockParticipation.setAppraisal(5.67);
        mockParticipation.setPlacement(1);
        mockParticipation.setStory("story");
        mockParticipation.setTitle("title");
        mockParticipation.setUuid(UUID.randomUUID());
        return mockParticipation;
    }

    public static ParticipationDto createMockParticipationDto() {
        var mockParticipationDto = new ParticipationDto();
        mockParticipationDto.setParticipationUuid(UUID.randomUUID());
        mockParticipationDto.setUser(createMockUser());
        mockParticipationDto.setContestUuid(UUID.randomUUID());
        mockParticipationDto.setPhoto("photo");
        mockParticipationDto.setAppraisal(5.67);
        mockParticipationDto.setStory("story");
        mockParticipationDto.setTitle("title");
        return mockParticipationDto;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setUuid(UUID.randomUUID());
        mockUser.setEmail("email@email.bg");
        mockUser.setFirstName("first name");
        mockUser.setLastName("last name");
        mockUser.setPassword("password");
        mockUser.setPoints(55);
        mockUser.setProfilePhoto("photo");
        mockUser.setRank(createMockRank());
        mockUser.setRole(createMockRole());
        mockUser.setUsername("username");
        return mockUser;
    }

    public static Juror createMockJuror() {
        var mockJuror = new Juror();
        mockJuror.setJurorId(1);
        mockJuror.setContestId(1);
        mockJuror.setUserId(1);
        return mockJuror;
    }

    public static Evaluation createMockEvaluation() {
        var mockEvaluation = new Evaluation();
        mockEvaluation.setEvaluationId(1);
        mockEvaluation.setJuror(createMockJuror());
        mockEvaluation.setParticipation(createMockParticipation());
        mockEvaluation.setDateCreated(LocalDateTime.now());
        mockEvaluation.setRating(8);
        mockEvaluation.setComment("comment");
        return mockEvaluation;
    }

    public static EvaluationDto createMockEvaluationDto() {
        var mockEvaluationDto = new EvaluationDto();
        mockEvaluationDto.setParticipationUuid(UUID.randomUUID());
        mockEvaluationDto.setRating(8);
        mockEvaluationDto.setComment("comment");
        return mockEvaluationDto;
    }

    public static  EvaluationResponseDto createMockEvaluationResponseDto() {
        var mockEvaluationDto = new EvaluationResponseDto();
        mockEvaluationDto.setComment("comment");
        mockEvaluationDto.setDateCreated(LocalDateTime.now());
        mockEvaluationDto.setRating(10);
        mockEvaluationDto.setParticipation(createMockParticipation());
        mockEvaluationDto.setJuror(createMockUser());
        return mockEvaluationDto;
    }

    public static Contest createMockContest() {
        var mockContest = new Contest();
        mockContest.setContestId(1);
        mockContest.setCreatedBy(createMockUser());
        mockContest.setDateCreated(LocalDateTime.now());
        mockContest.setUuid(UUID.fromString("dd021a6c-b223-42a7-ad15-f18165bce303"));
        mockContest.setTitle("title");
        mockContest.setCategory(createMockCategory());
        mockContest.setCoverPhoto("photo");
        mockContest.setFinished(false);
        mockContest.setGraded(false);
        mockContest.setPhaseOne(LocalDateTime.of(2023, 4, 23, 20, 54));
        mockContest.setPhaseTwo(mockContest.getPhaseOne().plusDays(2));
        return mockContest;
    }

    public static ContestResponseDto createMockContestDto() {
        var mockContestDto = new ContestResponseDto();

        mockContestDto.setCreatedBy(createMockUser().getUsername());
        mockContestDto.setDateCreated(LocalDateTime.now());
        mockContestDto.setContestUuid(UUID.fromString("dd021a6c-b223-42a7-ad15-f18165bce303"));
        mockContestDto.setTitle("title");
        mockContestDto.setCategoryName(createMockCategory().getName());
        mockContestDto.setCoverPhoto("photo");
        mockContestDto.setFinished(false);
        mockContestDto.setGraded(false);
        mockContestDto.setPhaseOne(LocalDateTime.of(2023, 4, 23, 20, 54));
        mockContestDto.setPhaseTwo(mockContestDto.getPhaseOne().plusDays(2));
        return mockContestDto;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setCategoryId(1);
        mockCategory.setName("category");
        return mockCategory;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setRoleId(1);
        mockRole.setName("Photo Junkie");
        return mockRole;
    }

    public static RoleDto createMockRoleDto() {
        var mockRoleDto = new RoleDto();
        mockRoleDto.setRoleName("Photo Junkie");
        return mockRoleDto;
    }

    public static Rank createMockRank() {
        var mockRank = new Rank();
        mockRank.setRankId(1);
        mockRank.setPoints(50);
        mockRank.setName("Junkie");
        return mockRank;
    }

    public static RankDto createMockRankDto() {
        var mockRankDto = new RankDto();
        mockRankDto.setRankName("Junkie");
        mockRankDto.setPoints(1);
        return mockRankDto;
    }

    public static UserRegistrationRequest createUserRegistrationRequest() {
        var mockRequest = new UserRegistrationRequest();
        mockRequest.setFirstName("first name");
        mockRequest.setLastName("last name");
        mockRequest.setEmail("email@email.bg");
        mockRequest.setUsername("username");
        mockRequest.setPassword("password");
        return mockRequest;
    }

    public static UserUpdateRequestDto createUserUpdateRequest() {
        var mockRequest = new UserUpdateRequestDto();
        mockRequest.setFirstName("first name");
        mockRequest.setLastName("last name");
        mockRequest.setPassword("password");
        return mockRequest;
    }
}
