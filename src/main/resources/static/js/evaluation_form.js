// Get all star icons
const stars = document.querySelectorAll('#ratingStars i');

// Add "checked" class to first star icon and set rating input value to 1
stars[0].classList.add('checked');
document.querySelector('#ratingInput').value = 1;

// Add event listener to each star icon
stars.forEach((star, index) => {
    star.addEventListener('click', () => {
        // Remove "checked" class from all star icons
        stars.forEach((s, i) => {
            if (i <= index) {
                s.classList.add('checked');
            } else {
                s.classList.remove('checked');
            }
        });
        // Set rating input value to the index of the clicked star plus one
        document.querySelector('#ratingInput').value = index + 1;
    });
});

// Get the form element
const form = document.querySelector('#evaluationForm');

// Add event listener to submit button
form.addEventListener('submit', (event) => {
    // Check if no star is selected
    if (document.querySelector('#ratingStars .checked') === null) {
        // Set rating input value to 1
        document.querySelector('#ratingInput').value = 1;
    }
    // Submit the form
    form.submit();
    event.preventDefault();
});


// Get the switch box
const switchBox = document.getElementById("flexSwitchCheckDefault");

// Get the comment textarea
const commentTextarea = document.getElementById("message-text");

// Add event listener to switch box
switchBox.addEventListener("change", function () {
    if (this.checked) {
        // Set the value of the switch input field to true
        document.getElementById("switchInput").value = "true";

        // Remove the "checked" class from all the star icons
        stars.forEach((s, i) => {
            s.classList.remove('checked');
        });

        // Set the rating input value to 0
        document.querySelector('#ratingInput').value = 0;

        // Set the comment textarea value to "Invalid category!"
        commentTextarea.value = "Invalid category!";
    } else {
        // Set the value of the switch input field to false
        document.getElementById("switchInput").value = "false";

        // Add "checked" class to first star icon and set rating input value to 1
        stars[0].classList.add('checked');
        document.querySelector('#ratingInput').value = 1;

        // Clear the comment textarea value
        commentTextarea.value = "";
    }
});

// Get the switch box, comment input, error message, and submit button
const commentInput = document.querySelector('#message-text');
const commentError = document.querySelector('#commentError');
const submitBtn = document.querySelector('#submitBtn');

// Function to check if the comment field is empty
function isCommentEmpty() {
    return commentInput.value.trim() === '';
}

// Function to show the error message
function showCommentError() {
    commentError.style.display = 'inline';
}

// Function to hide the error message
function hideCommentError() {
    commentError.style.display = 'none';
}

// Disable the submit button initially
submitBtn.disabled = true;

// Add event listener to the switch box to update the comment field validity
switchBox.addEventListener("change", function () {
    if (this.checked) {
        commentInput.value = "Invalid Category!";
        commentInput.setAttribute('readonly', 'readonly');
        submitBtn.disabled = false;
        hideCommentError();
    } else {
        commentInput.value = "";
        commentInput.removeAttribute('readonly');
        if (isCommentEmpty()) {
            submitBtn.disabled = true;
            showCommentError();
        } else {
            submitBtn.disabled = false;
            hideCommentError();
        }
    }
});

// Add event listener to the comment field to check if it's empty
commentInput.addEventListener('input', () => {
    if (isCommentEmpty() && !switchBox.checked) {
        submitBtn.disabled = true;
        showCommentError();
    } else {
        submitBtn.disabled = false;
        hideCommentError();
    }
});

// Add event listener to the form's submit event
form.addEventListener('submit', (event) => {
    if (isCommentEmpty() && !switchBox.checked) {
        event.preventDefault();
        showCommentError();
    }
});

if (form) {
    const submitBtn = document.getElementById('submitBtn');
    submitBtn.addEventListener('click', () => {
        form.submit();
    });
}
