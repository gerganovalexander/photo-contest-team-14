$(document).ready(function () {
    // Show the modal when the form is submitted
    $('form.regform').submit(function (e) {
        e.preventDefault(); // Prevent the form from submitting
        $('#participationModal').modal('show'); // Show the modal
    });

    // Handle the modal submit button click event
    $('#submitBtn').click(function () {
        $('form.regform').off('submit').submit(); // Remove the submit event handler and submit the form
    });

    // Handle the modal cancel button click event
    $('#cancelBtn').click(function () {
        $('#participationModal').modal('hide'); // Hide the modal
        $('form.regform').off('submit'); // Remove the submit event handler from the form
    });

    // $('#participationModal').on('hidden.bs.modal', function (e) {
    //     window.location.href = window.location.href;
    // });
});