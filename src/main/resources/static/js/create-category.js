function showNewCategoryForm() {
    document.getElementById("newCategoryForm").style.display = "block";
    document.getElementById("newCategoryName").required = true;

}

function createCategory() {
    var categoryName = document.getElementById("newCategoryName").value;
    if (categoryName != null && categoryName != "") {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/categories/create", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                alert("Category created successfully!");
                window.location.reload(); // reload page after creating new category
            }
        };
        xhr.send(JSON.stringify({name: categoryName}));
    } else {
        document.getElementById("newCategoryForm").classList.add("has-error");
        document.getElementById("newCategoryError").textContent = "Category name is required";
    }

    const createCategoryBtn = document.getElementById("createCategoryBtn");
    createCategoryBtn.addEventListener("click", (event) => {
        event.preventDefault();
        // Your code to create a new category goes here
        fetch('/js/create-category.js')
            .then(response => response.text())
            .then(data => console.log(data))
            .catch(error => console.error(error));
    });

    const createContestBtn = document.getElementById("createContestBtn");
    createContestBtn.addEventListener("click", (event) => {
        event.preventDefault();
        event.stopPropagation(); // Prevent event from propagating to outer form
        // Your code to create a new contest goes here
        console.log("Create contest button clicked");
    });
}
