// Get a reference to the next button
const nextBtn = document.querySelector('.next_btn');

// Disable the next button initially
nextBtn.disabled = true;

// Add event listeners to required fields
const titleInput = document.querySelector('#name');
titleInput.addEventListener('input', checkForm);
const photoInput = document.querySelector('input[type="file"]');
photoInput.addEventListener('change', checkForm);
const storyInput = document.querySelector('#story');
storyInput.addEventListener('input', checkForm);

// Check if all required fields are filled in and enable the next button if they are
function checkForm() {
    const pattern = /^[a-zA-Z0-9 ]+$/;
    const isValid = pattern.test(titleInput.value);
    if (titleInput.value && photoInput.files.length && storyInput.value && isValid) {
        nextBtn.disabled = false;
        clearErrors();
    } else {
        nextBtn.disabled = true;
        showErrors(isValid);
    }
    // if (titleInput.value && photoInput.files.length && storyInput.value) {
    //     nextBtn.disabled = false;
    //     clearErrors();
    // } else {
    //     nextBtn.disabled = true;
    //     showErrors();
    // }
}

// Show error messages for empty fields
function showErrors() {
    const errorElements = document.querySelectorAll('.error-message');
    errorElements.forEach((errorElement) => {
        const inputElement = errorElement.previousElementSibling;
        if (!inputElement.value && inputElement.type !== 'file') {
            const fieldName = inputElement.name.charAt(0).toUpperCase() + inputElement.name.slice(1);
            errorElement.textContent = `${fieldName} is required.`;
            errorElement.style.display = 'block';
        } else if (inputElement.type === 'file' && !photoInput.files.length) {
            errorElement.textContent = 'Photo is required.';
            errorElement.style.display = 'block';
        } else if (inputElement.name === 'title' && /[^a-zA-Z0-9 ]/.test(inputElement.value)) {
            errorElement.textContent = 'Title cannot contain special symbols.';
            errorElement.style.display = 'block';
        } else {
            errorElement.style.display = 'none';
            errorElement.textContent = '';
        }
    });
}

// Clear error messages
function clearErrors() {
    const errorElements = document.querySelectorAll('.error-message');
    errorElements.forEach((errorElement) => {
        errorElement.style.display = 'none';
        errorElement.textContent = '';
    });
}