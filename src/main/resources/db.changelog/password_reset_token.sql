alter table users
    add password_reset_token varchar(255) null;

alter table users
    add email_confirmation_token varchar(255) null;
