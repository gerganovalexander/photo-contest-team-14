INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Panayot', 'Zarov', 'pongo', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'pongo@company.com', 1, 3, 157,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '64a411e7-cf96-4e49-b860-2af6fe03e289', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Anelia', 'Ivanova', 'anche', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'anche@company.com', 1, 3, 160,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '4144421a-f1a1-4f2b-9d71-226e38117472', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Atanas', 'Gagulov', 'gagula', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','gagula@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '49031562-011b-4581-a664-33a2e0c6c2ff', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Nikolai', 'Ivanov', 'nikiSun', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy','nikiSun@company.com', 2, 4, 1200,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        'dc38eb2b-49d8-44c9-bd20-c4db0af54959', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Petar', 'Ivanov', 'padre', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi','padre@company.com', 2, 3, 500,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        'cb78743b-0e78-415a-9ad1-e40d7a8e0dbf', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Nikolai', 'Simeonov', 'kevinG', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC','kevinG@company.com', 2, 3, 500,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        'a1b29cea-da1b-4e75-b1ad-1504e40b1c2e', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Dayana', 'Kyoseva', 'dayana', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','dayana@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '2d2516c6-0366-4178-ae1c-465c212ab605', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Galin', 'Shterev', 'galcho', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','galcho@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        'b11d2900-7ff3-405c-98c0-32d9b6c2f0ee', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Simeon', 'Radev', 'monchi', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','monchi@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '95207fa3-32a7-482f-b7c5-3afbe4d65ee1', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Mikaela', 'Stamenova', 'mika', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','mika@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '405b83e2-0028-4bb3-99bb-4d4733b0b08d', 1, 0);
INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`,
                     `profile_photo`, `uuid`, `is_enabled`, `is_locked`)
VALUES ('Plamen', 'Kyosev', 'plamkata', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6','plamkata@company.com', 2, 1, 0,
        'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740',
        '8fdeab13-eeb2-4f4b-9e6d-c24a09d6c3f1', 1, 0);