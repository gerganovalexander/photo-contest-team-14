package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Juror;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface JurorService {

    Juror getByID(int jurorId);

    List<Juror> getAllByContestId(int contestId);

    Juror getByContestUuidAndUserUuid(UUID contestUuid, UUID userUuid);

    boolean findIfUserIsJury(String username);

    void create(UUID contestUuid, UUID userUuid);

    void create(int contestId, int userId);

    @Transactional
    void delete(Juror juror);
}
