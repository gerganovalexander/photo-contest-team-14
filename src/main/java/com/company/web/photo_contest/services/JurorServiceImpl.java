package com.company.web.photo_contest.services;

import com.company.web.photo_contest.models.Juror;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.repositories.ContestRepository;
import com.company.web.photo_contest.repositories.JurorRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import com.company.web.photo_contest.services.contracts.JurorService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class JurorServiceImpl implements JurorService {

    private final JurorRepository jurorRepository;

    private final UserRepository userRepository;

    private final ContestRepository contestRepository;

    @Override
    public Juror getByID(int jurorId) {
        return jurorRepository.findById(jurorId).orElseThrow(() -> new EntityNotFoundException("Juror not found"));
    }

    @Override
    public List<Juror> getAllByContestId(int contestId) {
        return jurorRepository.getAllByContestId(contestId);
    }

    @Override
    public boolean findIfUserIsJury(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("Juror not found"));
        Optional<Juror> potentialJuror = jurorRepository.findFirstByUserId(user.getUserId());
        if (potentialJuror.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Juror getByContestUuidAndUserUuid(UUID contestUuid, UUID userUuid) {
        int contestId = contestRepository.getContestByUuid(contestUuid)
                .orElseThrow(() -> new EntityNotFoundException("Contest not found")).getContestId();
        int userId = userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new EntityNotFoundException("User not found")).getUserId();
        return jurorRepository.getJurorByContestIdAndUserId(contestId, userId)
                .orElse(null);
    }

    @Transactional
    @Override
    public void create(UUID contestUuid, UUID userUuid) {
        int contestId = contestRepository.getContestByUuid(contestUuid)
                .orElseThrow(() -> new EntityNotFoundException("Contest not found")).getContestId();
        int userId = userRepository.findByUuid(userUuid)
                .orElseThrow(() -> new EntityNotFoundException("User not found")).getUserId();
        if (jurorRepository.getJurorByContestIdAndUserId(contestId, userId).isEmpty()) {
            Juror juror = new Juror();
            juror.setUserId(userId);
            juror.setContestId(contestId);
            jurorRepository.save(juror);
        } else {
            throw new EntityExistsException("Duplicate Juror");
        }
    }

    @Transactional
    @Override
    public void create(int contestId, int userId) {
        if (jurorRepository.getJurorByContestIdAndUserId(contestId, userId).isEmpty()) {
            Juror juror = new Juror();
            juror.setUserId(userId);
            juror.setContestId(contestId);
            jurorRepository.save(juror);
        } else {
            throw new EntityExistsException("Duplicate Juror");
        }
    }

    @Transactional
    @Override
    public void delete(Juror juror) {
        jurorRepository.delete(juror);
    }
}
