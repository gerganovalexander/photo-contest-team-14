package com.company.web.photo_contest.services.contracts;

import com.company.web.photo_contest.models.Category;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getCategoryById(Integer id);

    Category getCategoryByName(String categoryName);

    List<Category> findAllByName(String name);

    List<Category> filterByName(String name);

    @Transactional
    Category createCategory(Category category);

    @Transactional
    void deleteCategory(Category category);

}
