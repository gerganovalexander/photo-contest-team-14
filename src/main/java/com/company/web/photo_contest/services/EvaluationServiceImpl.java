package com.company.web.photo_contest.services;

import com.company.web.photo_contest.helpers.EvaluationMapper;
import com.company.web.photo_contest.models.*;
import com.company.web.photo_contest.modelsDto.EvaluationDto;
import com.company.web.photo_contest.modelsDto.EvaluationResponseDto;
import com.company.web.photo_contest.repositories.EvaluationRepository;
import com.company.web.photo_contest.repositories.JurorRepository;
import com.company.web.photo_contest.repositories.ParticipationRepository;
import com.company.web.photo_contest.repositories.UserRepository;
import com.company.web.photo_contest.services.contracts.EvaluationService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class EvaluationServiceImpl implements EvaluationService {

    private final EvaluationRepository evaluationRepository;

    private final ParticipationRepository participationRepository;

    private final EvaluationMapper evaluationMapper;

    private final UserRepository userRepository;

    private final JurorRepository jurorRepository;

    @Override
    public List<Evaluation> getAll() {
        return evaluationRepository.findAll();
    }

    @Override
    public List<EvaluationResponseDto> getAllEvaluationDtoByParticipation(UUID participationUuid) {
        Participation participation = participationRepository.findByUuid(participationUuid)
                .orElseThrow(() -> new EntityNotFoundException("Participation not found."));
        List<Evaluation> evaluationsList = evaluationRepository.findAllByParticipation(participation);
        List<EvaluationResponseDto> mappedList = new ArrayList<>();
        for (Evaluation evaluation : evaluationsList) {
            User evaluationJuror = userRepository.getReferenceById(evaluation.getJuror().getUserId());
            EvaluationResponseDto mappedEvaluation = evaluationMapper.convertToDto(participation, evaluation, evaluationJuror);
            mappedList.add(mappedEvaluation);
        }
        return mappedList;
    }

    @Override
    public Evaluation getById(int evaluationId) {
        return evaluationRepository.findById(evaluationId)
                .orElseThrow(() -> new EntityNotFoundException("Evaluation not found."));
    }

    @Override
    @Transactional
    public void create(EvaluationDto evaluationDto, String username) {
        Participation participation = participationRepository.findByUuid(evaluationDto.getParticipationUuid())
                .orElseThrow(() -> new EntityNotFoundException("Participation not found"));

        int contestId = participation.getContest().getContestId();

        int userId = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User not found")).getUserId();

        Juror juror = jurorRepository.getJurorByContestIdAndUserId(contestId, userId)
                .orElseThrow(() -> new EntityNotFoundException("Juror not found"));

        Evaluation evaluation = evaluationMapper.fromDto(participation, evaluationDto, juror);

        if (evaluationRepository.findByJurorAndParticipation(evaluation.getJuror(),
                evaluation.getParticipation()).isEmpty()) {
            evaluationRepository.save(evaluation);
            updateParticipationAppraisal(evaluation.getParticipation());
            updateParticipationPlacement(evaluation.getParticipation().getContest());
        } else {
            throw new EntityExistsException("Already evaluated by this juror");
        }
    }

    @Override
    @Transactional
    public void createDefaultEvaluation(UUID participationUuid) {
        Participation participation = participationRepository.findByUuid(participationUuid)
                .orElseThrow(() -> new EntityNotFoundException("Participation not found"));
        Contest contest = participation.getContest();
        if (contest.isFinished()) {
            int missedReviews = jurorRepository.getAllByContestId(contest.getContestId()).size() -
                    evaluationRepository.findAllByParticipation(participation).size();
            if (missedReviews != 0) {
                updateDefaultParticipationAppraisal(participation, missedReviews);
                updateParticipationPlacement(participation.getContest());
            }
        }
    }

    @Transactional
    protected void updateDefaultParticipationAppraisal(Participation participation, int missedReviews) {
        double rating = 3 * missedReviews;
        List<Integer> list = evaluationRepository
                .findAllByParticipation(participation)
                .stream().map(Evaluation::getRating).toList();
        for (Integer integer : list) {
            rating += integer;
        }
        double averageRating = rating / (list.size() + missedReviews);
        participation.setAppraisal(averageRating);
    }

    @Transactional
    protected void updateParticipationAppraisal(Participation participation) {
        List<Integer> list = evaluationRepository
                .findAllByParticipation(participation)
                .stream().map(Evaluation::getRating).toList();
        double rating = 0;
        for (Integer integer : list) {
            rating += integer;
        }
        double averageRating = rating / list.size();
        participation.setAppraisal(averageRating);
    }

    @Transactional
    protected void updateParticipationPlacement(Contest contest) {
        List<Participation> list = participationRepository.findAllByContestOrderByAppraisalDesc(contest);
        int currentPlacement = 1;
        double previousAppraisal = -1;
        for (Participation participation : list) {
            double currentAppraisal = participation.getAppraisal();
            if (currentAppraisal == previousAppraisal) {
                participation.setPlacement(currentPlacement - 1);
            } else {
                participation.setPlacement(currentPlacement);
                currentPlacement++;
            }
            previousAppraisal = currentAppraisal;
        }
    }
}
