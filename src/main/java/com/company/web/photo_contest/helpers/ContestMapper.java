package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.modelsDto.ContestCreateDto;
import com.company.web.photo_contest.modelsDto.ContestResponseDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;

@AllArgsConstructor
@Component
public class ContestMapper {

    private final AmazonS3UploadUtil amazonS3UploadUtil;

    public Contest fromDto(ContestCreateDto dto, String username) {
        Contest contest = new Contest();
        contest.setTitle(dto.getTitle());
        contest.setPhaseOne(dto.getPhaseOne());
        contest.setPhaseTwo(dto.getPhaseTwo());

        if (dto.getPhotoFile() != null && dto.getPhotoFile().getSize() != 0) {
            try {
                String convertPhotoUrl = amazonS3UploadUtil.photoFromCreateContest(contest.getTitle(),
                        username, contest.getUuid().toString(), dto.getPhotoFile());

                contest.setCoverPhoto(convertPhotoUrl);
            } catch (Exception e) {
                throw new UnsupportedOperationException("Unsupported file type");
            }
        } else if (!dto.getCoverPhotoUrl().isEmpty()) {
            contest.setCoverPhoto(dto.getCoverPhotoUrl());
        } else if (dto.getSelectCoverPhoto() != null) {
            contest.setCoverPhoto(dto.getSelectCoverPhoto());
        }
        return contest;
    }

    public ContestResponseDto viewContest(Contest contest) {
        ContestResponseDto contestResponseDto = new ContestResponseDto();

        contestResponseDto.setTitle(contest.getTitle());
        contestResponseDto.setCategoryName(contest.getCategory().getName());
        contestResponseDto.setDateCreated(contest.getDateCreated());
        contestResponseDto.setPhaseOne(contest.getPhaseOne());
        contestResponseDto.setPhaseTwo(contest.getPhaseTwo());
        contestResponseDto.setFinished(contest.isFinished());
        contestResponseDto.setCreatedBy(contest.getCreatedBy().getUsername());
        contestResponseDto.setCoverPhoto(contest.getCoverPhoto());
        contestResponseDto.setParticipants(Set.copyOf(contest.getParticipants()));
        contestResponseDto.setGraded(contest.isGraded());
        contestResponseDto.setContestUuid(contest.getUuid());

        return contestResponseDto;
    }
}
