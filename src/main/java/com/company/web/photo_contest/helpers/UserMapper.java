package com.company.web.photo_contest.helpers;

import com.company.web.photo_contest.models.Rank;
import com.company.web.photo_contest.models.Role;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.*;
import com.company.web.photo_contest.security.PasswordEncoder;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;

    public User convertToUserFromRegistrationDto(UserRegistrationRequest userDto, Role role, Rank rank) {
        User user = new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getEmail(),
                userDto.getUsername(),
                passwordEncoder.bCryptPasswordEncoder().encode(userDto.getPassword()),
                role,
                rank);

        return user;
    }

    public User convertToUserFromUpdateDto(User authenticatedUser, UserUpdateRequestDto userDto) {
        User user = new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                authenticatedUser.getEmail(),
                authenticatedUser.getUsername(),
                passwordEncoder.bCryptPasswordEncoder().encode(userDto.getPassword()),
                authenticatedUser.getRole(),
                authenticatedUser.getRank()
        );
        user.setUuid(authenticatedUser.getUuid());
        user.setUserId(authenticatedUser.getUserId());
        user.setPoints(authenticatedUser.getPoints());
        user.setEnabled(authenticatedUser.isEnabled());
        user.setLocked(authenticatedUser.isLocked());
        user.setProfilePhoto(userDto.getProfilePhoto());
        return user;
    }

    public UserUpdateRequestDto convertToUserUpdateDtoFromUser(User user) {
        UserUpdateRequestDto userUpdateRequestDto = new UserUpdateRequestDto();
        userUpdateRequestDto.setUsername(user.getUsername());
        userUpdateRequestDto.setPassword(user.getPassword());
        userUpdateRequestDto.setFirstName(user.getFirstName());
        userUpdateRequestDto.setLastName(user.getLastName());
        userUpdateRequestDto.setUserUuid(user.getUuid());
        userUpdateRequestDto.setProfilePhoto(user.getProfilePhoto());
        userUpdateRequestDto.setPoints(user.getPoints());
        userUpdateRequestDto.setEmail(user.getEmail());
        userUpdateRequestDto.setRole(user.getRole().getName());
        userUpdateRequestDto.setRank(user.getRank().getName());
        return userUpdateRequestDto;
    }

    public UserUpdateRequestDto convertUpdate(User user) {
        UserUpdateRequestDto userUpdateRequestDto = new UserUpdateRequestDto();
        userUpdateRequestDto.setUserUuid(user.getUuid());
        userUpdateRequestDto.setPoints(user.getPoints());
        userUpdateRequestDto.setEmail(user.getEmail());
        userUpdateRequestDto.setRank(user.getRank().getName());
        userUpdateRequestDto.setRole(user.getRole().getName());
        userUpdateRequestDto.setUsername(user.getUsername());
        return userUpdateRequestDto;
    }

    public Role convertToRoleFromCreateDto(RoleDto roleDto) {
        Role role = new Role();
        role.setName(roleDto.getRoleName());
        return role;
    }

    public Role convertToRoleFromUpdateDto(int roleId, RoleDto roleDto) {
        Role role = convertToRoleFromCreateDto(roleDto);
        role.setRoleId(roleId);
        return role;
    }

    public Rank convertToRankFromCreateDto(RankDto rankDto) {
        Rank rank = new Rank();
        rank.setName(rankDto.getRankName());
        rank.setPoints(rankDto.getPoints());
        return rank;
    }

    public Rank convertToRankFromUpdateDto(int rankId, RankDto rankDto) {
        Rank rank = convertToRankFromCreateDto(rankDto);
        rank.setRankId(rankId);
        return rank;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setRank(user.getRank().getName());
        userDto.setRole(user.getRole().getName());
        userDto.setPoints(user.getPoints());
        userDto.setProfilePhoto(user.getProfilePhoto());
        userDto.setUserUuid(user.getUuid());
        userDto.setPassword(user.getPassword());
        return userDto;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setProfilePhoto(userDto.getProfilePhoto());
        passwordEncoder.bCryptPasswordEncoder().encode(userDto.getPassword());
        return user;
    }

}
