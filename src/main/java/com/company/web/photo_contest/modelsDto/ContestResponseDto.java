package com.company.web.photo_contest.modelsDto;

import com.company.web.photo_contest.models.User;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
public class ContestResponseDto {

    private String title;

    private String categoryName;

    private LocalDateTime dateCreated;

    private LocalDateTime phaseOne;

    private LocalDateTime phaseTwo;

    private boolean isFinished;

    private Set<User> participants;

    private boolean isGraded;

    private String createdBy;

    private String coverPhoto;

    private UUID contestUuid;
}
