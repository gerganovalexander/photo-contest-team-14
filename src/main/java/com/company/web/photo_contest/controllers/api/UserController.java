package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.models.Rank;
import com.company.web.photo_contest.models.Role;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.*;
import com.company.web.photo_contest.services.contracts.RankService;
import com.company.web.photo_contest.services.contracts.RoleService;
import com.company.web.photo_contest.services.contracts.UserService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api")
public class UserController {

    private final UserService userService;
    private final RoleService roleService;
    private final RankService rankService;

    @GetMapping("/users")
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/users/{uuid}")
    public User getUserByUUid(@PathVariable UUID uuid) {
        try {
            return userService.getUserByUuid(uuid);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/users/username/{username}")
    public User getUserByUsername(@PathVariable String username) {
        try {
            return userService.getUserByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/users/filter")
    public List<User> filter(@RequestParam(required = false) String firstName,
                             @RequestParam(required = false) String lastName,
                             @RequestParam(required = false) String username) {
        return userService.findBy(firstName, lastName, username);
    }

    @PostMapping("/users")
    public User createUser(@Valid @RequestBody UserRegistrationRequest userRegistrationRequest) {
        try {
            return userService.createUser(userRegistrationRequest);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/users/{uuid}")
    public User updateUser(@PathVariable UUID uuid,
                           @Valid @RequestBody UserUpdateRequestDto userUpdateRequestDto,
                           Authentication authentication) {
        try {
            return userService.updateUser(uuid, userUpdateRequestDto, authentication.getName());
        } catch (AccessDeniedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/users/{uuid}")
    public void deleteUser(@PathVariable UUID uuid) {
        userService.deleteUser(uuid);
    }

    @PreAuthorize("hasRole('Organizer')")
    @PutMapping("/users/make-organizer/{uuid}")
    public void makeJunkieOrganizer(@PathVariable UUID uuid) {
        userService.makeJunkieOrganizer(uuid);
    }

    @PreAuthorize("hasRole('Organizer')")
    @PutMapping("/users/block/{uuid}")
    public void blockUser(@PathVariable UUID uuid) {
        userService.blockUser(uuid);
    }

    @PreAuthorize("hasRole('Organizer')")
    @PutMapping("/users/unblock/{uuid}")
    public void unblockUser(@PathVariable UUID uuid) {
        userService.unblockUser(uuid);
    }

    @PutMapping("/users/{uuid}/points")
    public void addPointsToUser(@PathVariable UUID uuid,
                                @Valid @RequestBody RankPointsUpdateRequestDto requestDto) {
        try {
            userService.addPointsToUser(uuid, requestDto.getPoints());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/roles")
    public List<Role> getAllRoles() {
        return roleService.getAll();
    }

    @GetMapping("/roles/{roleName}")
    public Role getRoleByName(@PathVariable String roleName) {
        try {
            return roleService.getRoleByName(roleName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PostMapping("/roles")
    public Role createRole(@Valid @RequestBody RoleDto roleDto) {
        try {
            return roleService.createRole(roleDto);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PutMapping("roles/{roleId}")
    public Role updateRole(@PathVariable int roleId,
                           @Valid @RequestBody RoleDto roleDto) {
        try {
            return roleService.updateRole(roleId, roleDto);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @DeleteMapping("/roles/{roleId}")
    public void deleteRole(@PathVariable int roleId) {
        try {
            roleService.deleteURole(roleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/ranks")
    public List<Rank> getAllRanks() {
        return rankService.getAll();
    }

    @GetMapping("/ranks/{rankName}")
    public Rank getRankByName(@PathVariable String rankName) {
        try {
            return rankService.getRankByName(rankName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PostMapping("/ranks")
    public Rank createRank(@Valid @RequestBody RankDto rankDto) {
        try {
            return rankService.createRank(rankDto);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PutMapping("ranks/{rankId}")
    public Rank updateRank(@PathVariable int rankId,
                           @Valid @RequestBody RankDto rankDto) {
        try {
            return rankService.updateRank(rankId, rankDto);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @DeleteMapping("/ranks/{rankId}")
    public void deleteRank(@PathVariable int rankId) {
        rankService.deleteRank(rankId);
    }

}
