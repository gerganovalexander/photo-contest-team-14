package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.modelsDto.LoginDto;
import com.company.web.photo_contest.modelsDto.UserRegistrationRequest;
import com.company.web.photo_contest.services.contracts.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;

    @PostMapping("/login")
    public String login(@RequestBody LoginDto loginDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return "Login successfully";
    }

    @PostMapping("/register")
    public String register(@RequestBody UserRegistrationRequest userRegistrationRequest) {
        userService.createUser(userRegistrationRequest);
        return "User register successfully.";
    }


}
