package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.models.Juror;
import com.company.web.photo_contest.services.contracts.JurorService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api")
public class JurorController {

    private final JurorService jurorService;

    @GetMapping("/contests/{contestId}/jurors")
    public List<Juror> getAllByContest(@PathVariable int contestId) {
        return jurorService.getAllByContestId(contestId);
    }

    @PostMapping("/contests/{contestUuid}/users/{userUuid}")
    public void create(@PathVariable UUID contestUuid, @PathVariable UUID userUuid) {
        try {
            jurorService.create(contestUuid, userUuid);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
