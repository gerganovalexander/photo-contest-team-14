package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.helpers.UserMapper;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.UserDto;
import com.company.web.photo_contest.security.SecurityUtil;
import com.company.web.photo_contest.services.contracts.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class HomeMvcController {

    private final UserService userService;
    private final ContestService contestService;
    private final ParticipationService participationService;
    private final EvaluationService evaluationService;
    private final JurorService jurorService;
    private final UserMapper userMapper;

    @ModelAttribute("contests")
    public List<Contest> getAllContestsByPhaseOne() {
        return contestService.getActiveContests();
    }

    @ModelAttribute("allPhotos")
    public int getAllParticipations() {
        return participationService.getAll().size();
    }

    @ModelAttribute("allVotes")
    public int getAllEvaluations() {
        return evaluationService.getAll().size();
    }

    @ModelAttribute("users")
    public int getAllUsers() {
        return userService.getAll().size();
    }

    @ModelAttribute("finishedContests")
    public int getAllFinishedContests() {
        return contestService.isFinishedCount();
    }

    @ModelAttribute("latestWinners")
    public List<Participation> getLastSixWinners() {
        return participationService.getWinners();
    }

    @GetMapping
    public String showHomePage(Model model) {
        User user = new User();
        String username = SecurityUtil.getSessionUser();
        if (username != null) {
            user = userService.getUserByUsername(username);
            model.addAttribute("user", user);
            UserDto userDto = userMapper.toDto(user);
            if (userDto.getRank().equals("Wise and Benevolent Photo Dictator")) {
                userDto.setRank("Photo Dictator");
            }
            model.addAttribute("userDto", userDto);
            boolean isCurrentUserJuror = jurorService.findIfUserIsJury(user.getUsername());
            model.addAttribute("isCurrentUserJuror", isCurrentUserJuror);
        }
        return "index";
    }
}
