package com.company.web.photo_contest.controllers.mvc;

import com.company.web.photo_contest.helpers.AmazonS3UploadUtil;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Juror;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import com.company.web.photo_contest.modelsDto.EvaluationDto;
import com.company.web.photo_contest.modelsDto.EvaluationResponseDto;
import com.company.web.photo_contest.modelsDto.ParticipationDto;
import com.company.web.photo_contest.services.contracts.*;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class ParticipationMvcController {

    public static final String JUROR_ALREADY_EVALUATED_PHOTO_MESSAGE = "You have already evaluated this photo!";
    private final AmazonS3UploadUtil amazonS3UploadUtil;
    private final ParticipationService participationService;
    private final UserService userService;
    private final ContestService contestService;
    private final EvaluationService evaluationService;
    private final JurorService jurorService;

    @ModelAttribute("user")
    public User getCurrentUser(Authentication authentication) {
        return userService.getUserByUsername(authentication.getName());
    }

    @ModelAttribute("isCurrentUserJuror")
    public boolean isCurrentUserJuror(Authentication authentication) {
        return jurorService.findIfUserIsJury(authentication.getName());
    }

    @GetMapping("/contest/{contestUuid}/upload")
    public String showParticipationPage(@PathVariable UUID contestUuid, Model model, Authentication authentication) {
        try {
            Contest contest = contestService.getContestByUuid(contestUuid);
            User user = userService.getUserByUsername(authentication.getName());
            model.addAttribute("contest", contest);
            model.addAttribute("participation", new ParticipationDto());
            model.addAttribute("user", user);
            return "upload-photo";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @PostMapping("/contest/{contestUuid}/upload")
    public String showPhotoPageUpload(@Valid @ModelAttribute("participation") ParticipationDto participationDto,
                                      @RequestParam("photoFile") @NotNull MultipartFile multipartFile,
                                      @PathVariable UUID contestUuid,
                                      Model model,
                                      Authentication authentication) {
        if (multipartFile.isEmpty()) {
            return "participation-fail";
        }
        try {
            String username = authentication.getName();
            Contest contest = contestService.getContestByUuid(contestUuid);
            String contestName = contestService.getContestByUuid(contestUuid).getTitle();
            String photoUrl = amazonS3UploadUtil.uploadPhotoFromParticipation(
                    participationDto, username, contestName, multipartFile);
            participationDto.setPhoto(photoUrl);
            participationDto.setContestUuid(contestUuid);
            participationService.create(participationDto, authentication.getName());

            model.addAttribute("contest", contest);
            model.addAttribute("photo", photoUrl);
            return "participation-success";

        } catch (EntityNotFoundException | EntityExistsException | IllegalArgumentException | IOException e) {
            model.addAttribute("error", e.getMessage());
            return "participation-fail";
        }
    }

    @GetMapping("/contest/{contestUuid}/participation/{participationUuid}")
    public String showContestParticipation(@PathVariable UUID contestUuid,
                                           @PathVariable UUID participationUuid,
                                           Model model,
                                           Authentication authentication,
                                           HttpServletRequest request) {
        try {
            User currentUser = userService.getUserByUsername(authentication.getName());
            model.addAttribute("currentUser", currentUser);
            Contest contest = contestService.getContestByUuid(contestUuid);
            Juror currentUserJuror = jurorService.getByContestUuidAndUserUuid(contest.getUuid(), currentUser.getUuid());
            boolean isCurrentUserJuror;
            if (currentUserJuror != null) {
                isCurrentUserJuror = true;
            } else {
                isCurrentUserJuror = false;
            }
            model.addAttribute("isCurrentUserJuror", isCurrentUserJuror);
            Participation participation = participationService.getByUuid(participationUuid);
            List<EvaluationResponseDto> evaluationsList = evaluationService.
                    getAllEvaluationDtoByParticipation(participationUuid);
            String currentUrl = request.getRequestURL().toString();
            model.addAttribute("currentUrl", currentUrl);
            model.addAttribute("contest", contest);
            model.addAttribute("participation", participation);
            model.addAttribute("evaluations", evaluationsList);
            model.addAttribute("evaluationDto", new EvaluationDto());
            return "single-participation-evaluation";
        } catch (EntityNotFoundException e) {
            return "404";
        }
    }

    @PostMapping("/contest/{contestUuid}/participation/{participationUuid}")
    public String showContestEvaluation(@PathVariable UUID contestUuid,
                                        @PathVariable UUID participationUuid,
                                        @Valid @ModelAttribute("evaluationDto") EvaluationDto evaluationDto,
                                        Model model,
                                        Authentication authentication) {
        try {
            User currentUser = userService.getUserByUsername(authentication.getName());
            model.addAttribute("currentUser", currentUser);
            Contest contest = contestService.getContestByUuid(contestUuid);
            Juror currentUserJuror = jurorService.getByContestUuidAndUserUuid(contest.getUuid(), currentUser.getUuid());
            boolean isCurrentUserJuror;
            if (currentUserJuror != null) {
                isCurrentUserJuror = true;
            } else {
                isCurrentUserJuror = false;
            }
            model.addAttribute("isCurrentUserJuror", isCurrentUserJuror);
            Participation participation = participationService.getByUuid(participationUuid);
            List<EvaluationResponseDto> evaluationsList = evaluationService.
                    getAllEvaluationDtoByParticipation(participationUuid);
            model.addAttribute("contest", contest);
            model.addAttribute("participation", participation);
            model.addAttribute("evaluations", evaluationsList);
            model.addAttribute("currentUser", currentUser);

            evaluationDto.setParticipationUuid(participationUuid);
            evaluationService.create(evaluationDto, currentUser.getUsername());

            return "redirect:/contest/{contestUuid}/participation/{participationUuid}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", e.getMessage());
            return "evaluation-message";
        } catch (EntityExistsException e) {
            model.addAttribute("message", JUROR_ALREADY_EVALUATED_PHOTO_MESSAGE);
            return "evaluation-message";
        }
    }

    @PostMapping("/contest/{contestUuid}/participation/{participationUuid}/delete")
    public String deleteParticipation(@PathVariable UUID contestUuid,
                                      @PathVariable UUID participationUuid,
                                      Model model,
                                      Authentication authentication) {
        try {
            User currentUser = userService.getUserByUsername(authentication.getName());
            model.addAttribute("currentUser", currentUser);
            Contest contest = contestService.getContestByUuid(contestUuid);
            Juror currentUserJuror = jurorService.getByContestUuidAndUserUuid(contest.getUuid(), currentUser.getUuid());
            boolean isCurrentUserJuror;
            if (currentUserJuror != null) {
                isCurrentUserJuror = true;
            } else {
                isCurrentUserJuror = false;
            }
            model.addAttribute("isCurrentUserJuror", isCurrentUserJuror);
            Participation participation = participationService.getByUuid(participationUuid);
            List<EvaluationResponseDto> evaluationsList = evaluationService.
                    getAllEvaluationDtoByParticipation(participationUuid);
            model.addAttribute("contest", contest);
            model.addAttribute("participation", participation);
            model.addAttribute("evaluations", evaluationsList);
            model.addAttribute("currentUser", currentUser);

            participationService.delete(participationUuid, currentUser.getUsername());

            return "redirect:/contest/single-contest?contestUuid={contestUuid}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", e.getMessage());
            return "evaluation-message";
        }
    }
}
