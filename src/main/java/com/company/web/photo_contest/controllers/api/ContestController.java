package com.company.web.photo_contest.controllers.api;

import com.company.web.photo_contest.models.Category;
import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.modelsDto.ContestCreateDto;
import com.company.web.photo_contest.modelsDto.ContestResponseDto;
import com.company.web.photo_contest.services.contracts.ContestService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/contest")
public class ContestController {

    private final ContestService contestService;

    @GetMapping
    public List<Contest> getAllContests() {
        List<Contest> contests = new ArrayList<>();
        contests = contestService.getAll();
        return contests;
    }

    @GetMapping("/{uuid}")
    public Contest getContestByUuid(@PathVariable UUID uuid) {
        try {
            return contestService.getContestByUuid(uuid);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{title}")
    public Contest getContestByTitle(@PathVariable String title) {
        try {
            return contestService.getContestsByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/title/{title}")
    public List<Contest> findByTitle(@PathVariable String title) {
        List<Contest> contests = new ArrayList<>();
        contests = contestService.findContestByTitle(title);
        return contests;
    }

    @GetMapping("/category/{category}")
    public List<Contest> findByCategory(@PathVariable String category) {
        List<Contest> contests = new ArrayList<>();
        contests = contestService.findContestByCategory(category);
        return contests;
    }

    @GetMapping("/finished/{isFinished}")
    public List<Contest> findIsFinished(@PathVariable Boolean isFinished) {
        List<Contest> contests = new ArrayList<>();
        contests = contestService.findIsFinished(isFinished);
        return contests;
    }

    @GetMapping("/phase-one/{phase}")
    public List<Contest> findByPhase(@PathVariable LocalDateTime phase) {
        List<Contest> contests = new ArrayList<>();
        contests = contestService.findContestByPhase(phase);
        return contests;
    }

    @GetMapping("/phase-two/{phase}")
    public List<Contest> findByPhaseTwo(@PathVariable LocalDateTime phase) {
        List<Contest> contests = new ArrayList<>();
        contests = contestService.findContestByPhaseTwo(phase);
        return contests;
    }

    @GetMapping("/filter")
    public List<Contest> filter
            (@RequestParam(required = false) String title,
             @RequestParam(required = false) Category category,
             @RequestParam(required = false) Boolean isFinished) {
        return contestService.findByFilter(title, category, isFinished);
    }

    @GetMapping("/filtering")
    public List<Contest> filtering(@RequestParam(required = false) String title,
                                   @RequestParam(required = false) Category category,
                                   @RequestParam(required = false) Boolean isFinished) {
        return contestService.filter(title, category, isFinished);
    }


    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PutMapping("/update/{uuid}")
    public ContestResponseDto update(@PathVariable UUID uuid, @RequestBody ContestCreateDto dto, Authentication authentication) {
        try {
            return contestService.updateContest(uuid, dto, authentication.getName());
        } catch (AccessDeniedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @PostMapping("/create")
    public void create(@Valid @RequestBody ContestCreateDto contestCreateDto, String username) {
        try {
            contestService.createContest(contestCreateDto, username);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_Organizer')")
    @DeleteMapping("/delete/{uuid}")
    public void delete(@PathVariable UUID uuid) {
        Contest contest = getContestByUuid(uuid);
        try {
            contestService.deleteContest(contest);
        } catch (EntityExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
