package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findRoleByName(String roleName);
}
