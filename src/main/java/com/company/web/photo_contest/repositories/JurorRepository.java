package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Juror;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface JurorRepository extends JpaRepository<Juror, Integer> {

    List<Juror> getAllByContestId(int contestId);

    Optional<Juror> getJurorByContestIdAndUserId(int contestId, int userId);

    Optional<Juror> findFirstByUserId(int userId);

    @Modifying
    void delete(Juror juror);
}
