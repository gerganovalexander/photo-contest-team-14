package com.company.web.photo_contest.repositories;

import com.company.web.photo_contest.models.Contest;
import com.company.web.photo_contest.models.Participation;
import com.company.web.photo_contest.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface ParticipationRepository extends JpaRepository<Participation, Integer> {

    Optional<Participation> findByUuid(UUID uuid);

    List<Participation> findAllByContestOrderByAppraisalDesc(Contest contest);

    List<Participation> findAllByContestOrderByPlacement(Contest contest);

    Optional<List<Participation>> findAllByContestUuid(UUID contestUuid);

    Optional<List<Participation>> findParticipationByTitle(String title);

    Optional<List<Participation>> findParticipationByPlacement(int placement);

    Optional<Participation> findParticipationByUserAndContest(User user, Contest contest);

    @Query("SELECT p FROM Participation p " +
            "JOIN p.contest c " +
            "WHERE c.isFinished = true AND p.placement = 1 " +
            "ORDER BY c.dateCreated DESC")
    List<Participation> findWinnersFromAllFinishedContestsLastSix();

    @Query("SELECT p FROM Participation p " +
            "JOIN p.contest c " +
            "WHERE c.isFinished = true AND p.placement IN (1, 2, 3) " +
            "ORDER BY p.contest.title, p.placement")
    Page<Participation> findAllWinners(Pageable pageable);

    Page<Participation> findAllByContestOrderByDateCreatedDesc(Contest contest, Pageable pageable);
}
