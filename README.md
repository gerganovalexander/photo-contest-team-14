# FrameFrenzy

[FrameFrenzy](http://photocontestteam14.eu-north-1.elasticbeanstalk.com/) WebSite

An Online Platform for Managing Photo Contests

[Click Here](https://streamable.com/2oc9bu) for a visual presentation of our user interface
[![Click Here](src/main/resources/static/images/project-presentation.jpg)](https://streamable.com/2oc9bu)
## Project Description

The FrameFrenzy project was developed by a team of talented photographers, who envisioned a user-friendly online platform to streamline the management of photo contests. The project consists of two main parts: the organizational part, where the application owners can effortlessly organize photo contests, and the user part, where photo enthusiasts can register and participate in the contests. The users with a high ranking can be invited to be jury members, adding a sense of community to the platform. FrameFrenzy offers a hassle-free experience for both organizers and participants, making it the ideal platform for managing and participating in photo contests.

# 🖥️ Website

![Webpage](src/main/resources/static/images/WebSite.PNG)

## Integrate with your tools

[Gitlab](https://gitlab.com/gerganovalexander/photo-contest-team-14)

## 📢 Public Part

✔ The public part must be accessible without authentication i.e., for anonymous users.

✔ Landing page – you can show the latest winning photos or something else that might be compelling for people to register.

✔ Login form – redirects to the private area of the application. Login requires username and password.

✔ Register form – registers someone as a Photo Junkie. Requires username, email, first name, last name, and password.
***

## 🔒 Private Part

✔ Accessible only if the user is authenticated
***

## Dashboard Page

✔ Dashboard page is different for Organizers and Photo Junkies

### For Organizers:

- There must be a way to setup a new Contest.
- There must be a way to view Contests which are in Phase I
- There must be a way to view Contests which are in Phase II
- There must be a way to view Contests which are Finished.
- Must be able to make other users organizers.
- Must be able to delete contest entry (due to offensive nature of the photo, comment etc.) Jury must be able to delete as well.
- The jury must not be able to participate within the same contest where they are jury.
- There must be a way to view Photo Junkies.
### Photo Junkies:

- There must be a way to view active Open contests.
- There must be a way to view contests that the junkie currently participates in.
- There should be a way to view finished contests that the junkie participated in.
***

## Contest Page

✔ The Contest Category is always visible.

### Phase I:

- The remaining time until Phase II should be displayed.
- The jury can view submitted photos but cannot rate them yet.
- Junkies see the enroll button if the contest is Open and they are not participating.
- If they are participating and have not uploaded a photo, they see a form for upload:

    - Title – short text (required)
    - Story – long text, which tells the captivating story of the photo (required)
    - Photo – file (required)

- Only one photo can be uploaded per participant. The photo, title, and story cannot be edited. Display a warning on submit that any data cannot be changed later.

### Phase II:

- Remaining time until Finish phase.
- Participants cannot upload anymore.
- The jury sees a form for each submitted photo.

    - Score (1-10) (required)
    - Comment (long text) (required)
    - Checkbox to mark that the photo does not fit the contest category. If the checkbox is selected, score 0 is assigned automatically and a Comment that the category is wrong. This is the only way to assign Score outside the [1, 10] interval.
    - Each juror can give one review per photo, if a photo is not reviewed, a default score of 3 is awarded.
  
### Finished:

- The jury can no longer review photos.
- Participants view their score and comments.
- In this phase, participants can also view the photos submitted by other users, along with their scores and comments by the Jury.
***

## Create Contest Page

✔ Create Contest Form – either a new page, or on the organizer’s dashboard. The following must be easy to set up.

- Title – text field (required and unique)
- Category – text field (required)
- Open Contest. Open means that everyone (except the jury can join)
- Phase I-time limit – anything from one day to one month
- Phase II time limit – anything from one hour to one day
- Select Jury – all users with Organizer role are automatically selected

    - Additionally, users with ranking Photo Master can also be selected, if the organizers decide
- Cover photo - a photo can be selected for a contest.

    - Option 1 – upload a cover photo.
    - Option 2 – paste the URL of an existing photo.
    - Option 3 – select the cover from previously uploaded photos.
    - The organizer must be able to choose between all three options and select the easiest for him/her
***
## Scoring

✔ Contest participation should award points. Points are accumulative, so being invited and subsequently winning will award 53 points totals.

- Joining open contest – 1 point
- Being invited by organizer – 3 points
- 3rd place – 20 points (10 points if shared 3rd)
- 2nd place – 35 points (25 points if shared 2nd)
- 1st place – 50 points (40 points if shared 1st)
- Finishing at 1st place with double the score of the 2nd (e.g., 1st has been awarded 8.6 points average, and 2nd is 4.3 or less) – 75 points
- In the case of a tie, positions are shared, so there can be more than one participant in 1st, 2nd, and 3rd places, all in the same contest.
***

## Ranking

- (0-50) points – Junkie
- (51 – 150) points – Enthusiast
- (151 – 1000) points – Master (can now be invited as jury)
- (1001 – infinity) points – Wise and Benevolent Photo Dictator (can still be jury)
***
## Forgotten Password

A customer has forgotten their password. They select the “Forgot password?” option and enter their email. The system checks if there is a registered user with that email. If there is one, he receives an email with a link to a page where he can enter a new password. The link should be accessible for a limited time only (say, an hour) and if accessed once, it should not be possible to access it again.
***
## Social Sharing

Participants that finish 1st, 2nd, or 3rd could have to option to share their achievement to a social media
***
## Rest API

[Swagger](https://app.swaggerhub.com/apis/tihomirshterev/FrameFrenzy/v1#/)

The REST API provides the following capabilities:
1. Users

    - CRUD Operations
    - List and search by username, first name or last name
2. Contests

    - CRUD Operations
    - Submit photo
    - Rate photo
    - List and filter by title, category, type and phase
3. Photos

    - CRD Operations
    - List and search by title
***
## Database Diagram
![Diagram](src/main/resources/static/images/DB Diagram.PNG)
***
## 👷 Built with:
- Java

- Unit testing with Mockito framework

- Spring Boot with Spring MVC and Spring Security

- Spring Data JPA

- Thymeleaf

- Hibernate

- SQL & MariaDB

- AWS S3 Bucket

- AWS Elastic Beanstalk

- AWS RDS

- Liquibase

- Java Mail Sender

  
Used front-end technologies:

- HTML 5

- CSS

- JavaScript

- Bootstrap

***
## 📖🖋️📜 Authors

![Tihomir](src/main/resources/static/images/team/tisho.jpg)

- Tihomir Shterev - ⛓ [Gitlab](https://gitlab.com/shtereF)
***

![Mariana](src/main/resources/static/images/team/Mary.jpg)

- Mariana Lenkova - ⛓ [Gitlab](https://gitlab.com/lenkova_m)
***

![Alexander](src/main/resources/static/images/team/Alex.jpg)

- Alexander Gerganov - ⛓ [Gitlab](https://gitlab.com/gerganovalexander)
***
