-- --------------------------------------------------------
-- Host:                         backedup-photocontestteam14.c8qxtum6qc35.eu-north-1.rds.amazonaws.com
-- Server version:               10.5.18-MariaDB-log - managed by https://aws.amazon.com/rds/
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table photo-contest.categories: ~8 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `name`) VALUES
	(6, 'Animals'),
	(7, 'Nature'),
	(8, 'People'),
	(9, 'Interior Details'),
	(10, 'Food'),
	(11, 'Day and Night'),
	(12, 'Buildings'),
	(13, 'Sports');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table photo-contest.contests: ~18 rows (approximately)
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` (`contest_id`, `title`, `category_id`, `date_created`, `phase_one`, `phase_two`, `is_finished`, `user_id`, `is_graded`, `uuid`, `cover_photo`) VALUES
	(14, 'Birds', 6, '2023-05-01 14:20:23', '2023-05-06 17:20:00', '2023-05-07 10:00:00', 0, 8, 0, '694cfe70-e905-435f-94c9-cb87c4ab32ad', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/694cfe70-e905-435f-94c9-cb87c4ab32ad/Birds_pesho.jpg'),
	(16, 'Sea Life', 6, '2023-05-03 13:39:17', '2023-05-03 21:54:00', '2023-05-03 23:12:00', 1, 8, 1, '17b010d5-f72c-4df4-b952-09f9688e9c0b', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/17b010d5-f72c-4df4-b952-09f9688e9c0b/Sea Life_pesho.jpg'),
	(17, 'Dogs', 6, '2023-05-03 13:40:36', '2023-05-03 21:54:00', '2023-05-03 23:12:00', 1, 8, 1, '894fcbc8-eee7-4d89-84de-92fc676b2c83', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/894fcbc8-eee7-4d89-84de-92fc676b2c83/Dogs_pesho.jpg'),
	(18, 'Cats', 6, '2023-05-03 13:41:35', '2023-05-03 21:55:00', '2023-05-03 23:36:00', 1, 8, 1, '6f3786d5-4927-446c-9f33-e91b19419cd3', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/6f3786d5-4927-446c-9f33-e91b19419cd3/Cats_pesho.jpg'),
	(19, 'Majestic Landscapes', 7, '2023-05-03 13:44:40', '2023-05-03 21:55:00', '2023-05-03 23:13:00', 1, 7, 1, '6898192a-e538-4bb9-8959-2bd8c85b688a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/6898192a-e538-4bb9-8959-2bd8c85b688a/Majestic Landscapes_ivanUser.jpg'),
	(20, 'People at Work ', 8, '2023-05-03 17:30:50', '2023-05-04 18:51:00', '2023-05-05 17:26:00', 0, 43, 0, '19dc2162-f6de-4be1-93ab-42beed0b82de', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/19dc2162-f6de-4be1-93ab-42beed0b82de/People at Work _mitko.jpg'),
	(21, 'Interior Design', 9, '2023-05-03 17:51:42', '2023-05-05 17:42:00', '2023-05-06 05:43:00', 0, 43, 0, '04756156-acce-4514-8045-ee480806f207', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/04756156-acce-4514-8045-ee480806f207/Interior Design_mitko.jpg'),
	(22, 'Bread', 10, '2023-05-04 10:44:19', '2023-05-06 10:43:00', '2023-05-07 05:43:00', 0, 43, 0, '47ec37e7-4024-4abd-8afc-ab594c401cf2', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/22ede79a-7c4e-4971-a918-7cfda84da5fa/Bread_mitko.jpg'),
	(23, 'Desserts', 10, '2023-05-04 10:45:56', '2023-05-07 10:45:00', '2023-05-08 05:45:00', 0, 38, 0, '81f60414-7f62-44fe-92e3-3dae931bcea1', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/61d6ebad-0da6-4ba8-b3c1-6229adb4f71f/Desserts_mitko.jpg'),
	(24, 'Lights at Night', 11, '2023-05-04 10:54:34', '2023-05-10 10:53:00', '2023-05-11 04:40:00', 0, 38, 0, '31a145ca-2615-4a22-b19b-09ff3a60d1df', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/31a145ca-2615-4a22-b19b-09ff3a60d1df/Lights at Night_bibi.jpg'),
	(25, 'Flowers', 7, '2023-05-04 14:26:21', '2023-05-04 16:55:00', '2023-05-05 20:25:00', 0, 49, 0, '3495024e-774e-4474-8fc7-562c0d03b614', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/3495024e-774e-4474-8fc7-562c0d03b614/Flowers_pongo.jpg'),
	(26, 'Castles', 12, '2023-05-04 14:31:26', '2023-05-06 14:30:00', '2023-05-06 19:30:00', 0, 49, 0, 'd5520f69-56a7-4ff9-bab8-ea6de70226fc', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/d5520f69-56a7-4ff9-bab8-ea6de70226fc/Castles_pongo.jpg'),
	(27, 'Sunrises', 7, '2023-05-04 14:36:01', '2023-05-05 18:39:00', '2023-05-06 10:35:00', 0, 49, 0, 'fdf1b03e-6b00-48a6-94ab-aeaadd3afcd1', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/fdf1b03e-6b00-48a6-94ab-aeaadd3afcd1/Sunrises_pongo.jpg'),
	(28, 'Wild Animals', 6, '2023-05-04 14:57:15', '2023-05-04 16:55:00', '2023-05-09 05:56:00', 0, 50, 0, '86dc7c52-1a55-4ae4-bb5e-c74c4a5d6f8b', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/86dc7c52-1a55-4ae4-bb5e-c74c4a5d6f8b/Wild Animals_anche.jpg'),
	(29, 'Beaches', 7, '2023-05-04 15:00:54', '2023-05-04 16:55:00', '2023-05-08 12:00:00', 0, 50, 0, '7233909a-bc02-47ab-8f6b-241b5bb24f11', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/7233909a-bc02-47ab-8f6b-241b5bb24f11/Beaches_anche.jpg'),
	(31, 'Ways We Fly', 13, '2023-05-04 17:41:44', '2023-05-25 19:41:00', '2023-05-26 14:41:00', 0, 43, 0, 'abb4e438-5359-4e59-958a-ea31e2b81323', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/abb4e438-5359-4e59-958a-ea31e2b81323/Ways We Fly_mitko.jpg'),
	(33, 'American Sports', 13, '2023-05-04 19:14:17', '2023-05-26 19:13:00', '2023-05-27 16:13:00', 0, 43, 0, '4a5e77e9-41af-4940-8421-1aa61308930a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/407c5d95-6fb4-4433-a567-5e13d96c1d1b/American Sports_mitko.jpg'),
	(34, 'Monkeys', 6, '2023-05-04 16:24:38', '2023-05-04 19:26:00', '2023-05-04 19:30:00', 1, 49, 1, 'ba876b66-4321-4000-ab47-f51b512e4b2a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/ba876b66-4321-4000-ab47-f51b512e4b2a/Monkeys_pongo.jpg');
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;

-- Dumping data for table photo-contest.DATABASECHANGELOG: ~15 rows (approximately)
/*!40000 ALTER TABLE `DATABASECHANGELOG` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOG` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
	('18', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:33', 1, 'EXECUTED', '8:1985fd2088e122d78200cb344c862e65', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('1', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:34', 2, 'EXECUTED', '8:353df08fb6172c34083d81c4abc93916', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('3', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:34', 3, 'EXECUTED', '8:6d9a451e819bc5df80b7e819b7e65513', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('4', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:36', 4, 'EXECUTED', '8:fed026ad80a61ecc4b3aef948528e726', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('5', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:36', 5, 'EXECUTED', '8:3c87ea1a3407c5edffd07f8693aeecae', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('10', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:36', 6, 'EXECUTED', '8:6b69f33d8e8e2d85dcaaa5957f6baaa6', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('12', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:37', 7, 'EXECUTED', '8:951b713844363351ae6f1bd569b44b05', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('14', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:37', 8, 'EXECUTED', '8:ad63c8819b2c25fc8e35b6d7786a0bb9', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('20', 'team', 'db.changelog/db.changelog.xml', '2023-05-01 13:22:39', 9, 'EXECUTED', '8:36f7069e40207f4a5a47ac146095d5cc', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '2947353575'),
	('15', 'team', 'db.changelog/db.changelog.xml', '2023-05-03 14:05:02', 10, 'EXECUTED', '8:d097962c473f4485aa3f111524ef803c', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '3122700072'),
	('26', 'team', 'db.changelog/db.changelog.xml', '2023-05-04 09:10:35', 11, 'EXECUTED', '8:8d13f43275319caad65cd62a06abb68b', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '3191432695'),
	('25', 'team', 'db.changelog/db.changelog.xml', '2023-05-04 09:30:12', 12, 'EXECUTED', '8:32ba4b35aa7820340193e478fcee833e', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '3192612045'),
	('27', 'team', 'db.changelog/db.changelog.xml', '2023-05-04 14:03:10', 13, 'EXECUTED', '8:373f3991474c8bed6ace71143e3430e3', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '3208989391'),
	('28', 'team', 'db.changelog/db.changelog.xml', '2023-05-04 14:27:19', 14, 'EXECUTED', '8:d27cdd10641d397716e5e1024a660a89', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '3210436466'),
	('30', 'team', 'db.changelog/db.changelog.xml', '2023-05-04 14:52:39', 15, 'EXECUTED', '8:a040fe4de05b2ba153e050ed1adeaa9b', 'sqlFile', '', NULL, '4.17.2', NULL, NULL, '3211957748');
/*!40000 ALTER TABLE `DATABASECHANGELOG` ENABLE KEYS */;

-- Dumping data for table photo-contest.DATABASECHANGELOGLOCK: ~0 rows (approximately)
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOGLOCK` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
	(1, b'0', NULL, NULL);
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` ENABLE KEYS */;

-- Dumping data for table photo-contest.evaluations: ~90 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
INSERT INTO `evaluations` (`evaluation_id`, `rating`, `comment`, `date_created`, `participation_id`, `juror_id`) VALUES
	(1, 7, 'I like it.', '2023-05-03 22:23:34', 35, 9),
	(2, 5, 'Nice!', '2023-05-03 22:24:47', 40, 9),
	(3, 6, 'Good!', '2023-05-03 22:25:40', 30, 9),
	(4, 10, 'This is a really nice photo!', '2023-05-03 22:26:35', 25, 9),
	(5, 8, 'Wow, pretty nice!', '2023-05-03 22:27:05', 20, 9),
	(6, 8, 'Beautiful!', '2023-05-03 22:27:38', 8, 9),
	(7, 8, 'Cute!', '2023-05-03 22:28:30', 41, 12),
	(8, 9, 'Adorable!', '2023-05-03 22:28:59', 36, 12),
	(9, 6, 'Hungry! Haha', '2023-05-03 22:29:34', 31, 12),
	(10, 9, 'What a beauty', '2023-05-03 22:30:06', 27, 12),
	(11, 8, 'A true wolf!', '2023-05-03 22:30:40', 21, 12),
	(12, 10, 'A black cocker spaniel. Thats rare!', '2023-05-03 22:31:08', 15, 12),
	(13, 8, 'Beautiful cat!', '2023-05-03 22:32:47', 42, 15),
	(14, 10, 'The eyes...', '2023-05-03 22:33:34', 37, 15),
	(15, 6, 'This cat has the appearance! ', '2023-05-03 22:34:10', 32, 15),
	(16, 9, 'Black tiger!', '2023-05-03 22:34:52', 22, 15),
	(17, 1, 'I am not so sure that this photo is legit', '2023-05-03 22:35:48', 6, 15),
	(18, 4, 'This guy looks angry!', '2023-05-03 22:36:28', 4, 15),
	(19, 9, 'Beautiful sunset!', '2023-05-03 22:40:03', 43, 17),
	(20, 8, 'What a stunning view!', '2023-05-03 22:40:54', 38, 17),
	(21, 8, 'Where is this really? ', '2023-05-03 22:42:06', 33, 17),
	(22, 6, 'Nice!', '2023-05-03 22:42:40', 28, 17),
	(23, 9, 'Stunning!', '2023-05-03 22:43:14', 24, 17),
	(24, 4, 'You could have taken a better angle!', '2023-05-03 22:44:04', 16, 17),
	(25, 10, 'Snowy!', '2023-05-03 22:44:40', 11, 17),
	(26, 6, 'Nice!', '2023-05-03 22:48:18', 41, 10),
	(27, 9, 'Cutie! Little puff!', '2023-05-03 22:49:04', 36, 10),
	(28, 7, 'Who is hungry?', '2023-05-03 22:49:40', 31, 10),
	(29, 9, 'Wow!', '2023-05-03 22:50:12', 27, 10),
	(30, 10, 'Wow!', '2023-05-03 22:50:46', 15, 10),
	(31, 10, 'Wow! Beauty!', '2023-05-03 22:51:30', 37, 13),
	(32, 8, 'She looks concentrated', '2023-05-03 22:52:13', 32, 13),
	(33, 10, 'Black puffy tiger!', '2023-05-03 22:52:47', 22, 13),
	(34, 7, 'Calm, but angry!', '2023-05-03 22:53:24', 4, 13),
	(35, 8, 'It may be called purple-tail', '2023-05-03 22:55:16', 35, 7),
	(36, 6, 'Wow! Looks terrific', '2023-05-03 22:55:49', 30, 7),
	(37, 10, 'Truly astonishing!', '2023-05-03 22:56:20', 25, 7),
	(38, 3, 'You are too close to this bear!', '2023-05-03 22:57:05', 20, 7),
	(39, 9, 'Beautiful star!', '2023-05-03 22:57:34', 8, 7),
	(40, 10, 'Pure beauty!', '2023-05-03 22:58:12', 15, 11),
	(41, 10, 'Wow! Truly a big beauty!', '2023-05-03 22:58:45', 27, 11),
	(42, 8, 'Who is a good boy?', '2023-05-03 22:59:22', 31, 11),
	(43, 9, 'Oooo, adorable little creature!', '2023-05-03 23:00:03', 36, 11),
	(44, 9, 'Wow!', '2023-05-03 23:00:46', 43, 16),
	(45, 6, 'Maybe too quiet', '2023-05-03 23:01:25', 38, 16),
	(46, 7, 'Nice!', '2023-05-03 23:01:53', 33, 16),
	(47, 7, 'Beautiful!', '2023-05-03 23:02:29', 28, 16),
	(48, 10, 'Beautiful sun and forest!', '2023-05-03 23:03:15', 11, 16),
	(49, 10, 'Really, those eyes are beautiful!', '2023-05-03 23:04:32', 37, 14),
	(50, 9, 'Highly Concentrated', '2023-05-03 23:05:19', 42, 14),
	(51, 0, 'Invalid Category!', '2023-05-03 23:05:44', 6, 14),
	(52, 8, 'Hehehe, this is cute!', '2023-05-03 23:06:31', 22, 14),
	(53, 10, 'Wow! Really nice shot!', '2023-05-03 23:07:19', 25, 8),
	(54, 9, 'Definately deserves a good rating! ', '2023-05-03 23:08:22', 8, 8),
	(55, 0, 'Invalid Category!', '2023-05-03 23:09:03', 40, 8),
	(56, 5, 'She looks calm', '2023-05-03 23:09:42', 30, 8),
	(57, 7, 'Not really a flower, but it is still beautiful.', '2023-05-04 17:01:12', 57, 42),
	(58, 8, 'Next time you can fix the colours a little bit. ', '2023-05-04 17:02:53', 55, 42),
	(59, 10, 'Wonderful picture!', '2023-05-04 17:04:20', 48, 42),
	(60, 0, 'Invalid Category!', '2023-05-04 17:06:00', 44, 19),
	(61, 9, 'You definitely caught the moment. Great job!', '2023-05-04 17:08:05', 26, 19),
	(62, 10, 'I like the message that this photo brings.', '2023-05-04 17:09:56', 18, 19),
	(63, 6, 'I don\'t see anything special here and can\'t give higher rating.', '2023-05-04 17:12:12', 13, 19),
	(64, 8, 'That\'s what I call extreme photography.', '2023-05-04 17:13:33', 64, 57),
	(65, 10, 'Wow! I don\'t have the words to describe how good this photo is. ', '2023-05-04 17:15:01', 60, 57),
	(66, 8, 'Like it!', '2023-05-04 17:15:56', 53, 57),
	(67, 7, 'The photo brings me back memories about family vacations. Nice one!', '2023-05-04 17:18:26', 62, 64),
	(68, 9, 'Good for a first photo! Keep up the good work!', '2023-05-04 17:20:00', 61, 64),
	(69, 6, 'Nothing special really. ', '2023-05-04 17:20:51', 56, 64),
	(70, 7, 'Like it!', '2023-05-04 17:21:52', 51, 64),
	(71, 8, 'I like the colours!', '2023-05-04 17:24:44', 57, 43),
	(72, 6, 'Not really professional photo.', '2023-05-04 17:26:24', 55, 43),
	(73, 10, 'Beautiful!', '2023-05-04 17:27:17', 48, 43),
	(74, 9, 'Scary!', '2023-05-04 17:28:33', 64, 58),
	(75, 10, 'Perfection!', '2023-05-04 17:29:47', 60, 58),
	(76, 9, 'Very nice picture!', '2023-05-04 17:33:01', 53, 58),
	(77, 7, 'Like it!', '2023-05-04 17:38:54', 62, 65),
	(78, 10, 'Is that really your first photo? You have a talent!', '2023-05-04 17:39:46', 61, 65),
	(79, 5, 'Casual photo', '2023-05-04 17:40:25', 56, 65),
	(80, 9, 'Love that beach and mountain view!', '2023-05-04 17:41:17', 51, 65),
	(81, 8, 'My favorite tree! The photo is good as well :)', '2023-05-04 17:44:59', 57, 44),
	(82, 7, 'Nice photo, but it\'s too bright. Next time you can make it look better.', '2023-05-04 17:46:42', 55, 44),
	(83, 10, 'Perfection!', '2023-05-04 17:47:15', 48, 44),
	(84, 7, 'You could have made the crocodile a little darker. Now at some places it is the same as the sand. ', '2023-05-04 17:50:25', 64, 59),
	(85, 10, 'If somebody ask me to show the perfect picture, I\'ll show this one.', '2023-05-04 17:51:36', 60, 59),
	(86, 9, 'Simple but very beautiful photo.', '2023-05-04 17:52:34', 53, 59),
	(87, 8, 'I like it a lot! Good job!', '2023-05-04 17:53:56', 62, 66),
	(88, 10, 'First photo? Really?', '2023-05-04 17:55:38', 61, 66),
	(89, 7, 'Fine photo.', '2023-05-04 17:56:30', 56, 66),
	(90, 8, 'This one looks very nice. I like it !', '2023-05-04 17:57:35', 51, 66);
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping data for table photo-contest.jurors: ~78 rows (approximately)
/*!40000 ALTER TABLE `jurors` DISABLE KEYS */;
INSERT INTO `jurors` (`juror_id`, `user_id`, `contest_id`) VALUES
	(4, 10, 14),
	(5, 7, 14),
	(7, 11, 16),
	(8, 12, 16),
	(9, 7, 16),
	(10, 10, 17),
	(11, 11, 17),
	(12, 7, 17),
	(13, 10, 18),
	(14, 12, 18),
	(15, 7, 18),
	(16, 11, 19),
	(17, 8, 19),
	(18, 7, 20),
	(19, 8, 20),
	(20, 38, 20),
	(21, 10, 21),
	(22, 35, 21),
	(23, 7, 21),
	(24, 8, 21),
	(25, 38, 21),
	(26, 10, 22),
	(27, 7, 22),
	(28, 8, 22),
	(29, 38, 22),
	(30, 12, 23),
	(31, 7, 23),
	(32, 8, 23),
	(33, 43, 23),
	(34, 10, 24),
	(35, 11, 24),
	(36, 12, 24),
	(37, 35, 24),
	(38, 7, 24),
	(39, 8, 24),
	(40, 43, 24),
	(41, 7, 25),
	(42, 8, 25),
	(43, 38, 25),
	(44, 43, 25),
	(45, 50, 25),
	(46, 7, 26),
	(47, 8, 26),
	(48, 38, 26),
	(49, 43, 26),
	(50, 50, 26),
	(51, 7, 27),
	(52, 8, 27),
	(53, 38, 27),
	(54, 43, 27),
	(55, 50, 27),
	(56, 7, 28),
	(57, 8, 28),
	(58, 38, 28),
	(59, 43, 28),
	(60, 49, 28),
	(61, 11, 29),
	(62, 52, 29),
	(63, 7, 29),
	(64, 8, 29),
	(65, 38, 29),
	(66, 43, 29),
	(67, 49, 29),
	(73, 7, 31),
	(74, 8, 31),
	(75, 38, 31),
	(76, 49, 31),
	(77, 50, 31),
	(83, 7, 33),
	(84, 8, 33),
	(85, 38, 33),
	(86, 49, 33),
	(87, 50, 33),
	(88, 7, 34),
	(89, 8, 34),
	(90, 38, 34),
	(91, 43, 34),
	(92, 50, 34);
/*!40000 ALTER TABLE `jurors` ENABLE KEYS */;

-- Dumping data for table photo-contest.participations: ~63 rows (approximately)
/*!40000 ALTER TABLE `participations` DISABLE KEYS */;
INSERT INTO `participations` (`participation_id`, `title`, `story`, `date_created`, `contest_id`, `user_id`, `appraisal`, `placement`, `uuid`, `photo`) VALUES
	(1, 'Nature in its finest form', 'This little guy right here got me inspired to pick up my camera and take the perfect shot of him. He was brilliant!', '2023-05-01 14:22:37', 14, 14, 0, 0, '17531a8a-05da-4567-801b-5000a7224460', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Nature in its finest form_koko.jpg'),
	(2, 'Stunning beauty', 'The American Goldfinch is a small, brightly colored bird found across North America. With its bright yellow plumage and black wings, this little bird is a delight to watch as it flits from tree to tree in search of food. Known for its distinctive song, the American Goldfinch is a favorite of birdwatchers and nature lovers alike. During breeding season, the male\'s plumage becomes even brighter, making it a truly stunning sight to behold.', '2023-05-03 13:53:13', 14, 11, 0, 0, 'f7a29778-446c-41f5-b432-d347ef0cfb65', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Stunning beauty_veselaUser.jpg'),
	(3, 'Hot Shoeing', 'Farrier hot shoeing a horse. This is the process of building the shoe in the forge and then applying it to the horse\'s hoof while still very hot to burn off any slight unevenness in the hoof and therefore creating a perfect seat for the shoe. This process when done correctly does not hurt the horse in any way; however, the extremely hot shoe is very close to the farrier\'s hands!', '2023-05-03 18:00:19', 20, 36, 0, 4, '576f3581-85c3-4b46-be80-8385fc8c9135', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /Hot Shoeing_veliUser.jpg'),
	(4, 'Peaceful Predator', 'My beautiful cat in a rainy morning.', '2023-05-03 18:04:09', 18, 11, 4.66667, 5, '4db75b87-738b-4219-88d3-a4b377490cdf', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Cats/Peaceful Predator_veselaUser.jpg'),
	(5, 'The bouquet', 'and the books...', '2023-05-03 19:11:05', 21, 36, 0, 0, 'dc1e4e2e-7f8e-486c-bfaf-d6182026295a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Interior Design/The bouquet_veliUser.jpg'),
	(6, 'Little poser', 'Taking it easy', '2023-05-03 19:15:55', 18, 36, 1.33333, 6, 'dc4d3c3a-5c7c-4a81-869f-2818dc9890ad', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Cats/Little poser_veliUser.jpg'),
	(7, 'Welding', 'Welding', '2023-05-03 19:19:04', 20, 35, 0, 4, '1b0def36-24fe-4b8b-82aa-a0f5d057cfcf', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /Welding_zdravec.jpg'),
	(8, 'Shining star', 'I was scuba diving last week when I saw this guy wandering around. I grabbed my camera and took the shot without thinking.', '2023-05-03 16:27:20', 16, 10, 8.66667, 2, 'ffa40008-bf19-47e4-8eb1-a36ab7f867c8', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sea Life/Shining star_monikaUser.jpg'),
	(9, 'A wooden table on a rusty old car', 'A wooden table on a rusty old car.', '2023-05-03 19:28:49', 21, 44, 0, 0, '22a859f6-40b4-46bd-ba3d-2e6594b4f5ae', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Interior Design/A wooden table on a rusty old car_meto.jpg'),
	(10, 'Black and white pottery', 'Shaping the future with muddy hands...', '2023-05-03 19:33:57', 20, 44, 0, 4, '97227bb0-4e7e-4698-aed6-6dac9c7a6d34', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /Black and white pottery_meto.jpg'),
	(11, 'Winter hike in the woods', 'I was with my wife on a hike in the near forest when she told me to just look around. I was thrilled. Astonishing.', '2023-05-03 19:35:43', 19, 10, 10, 1, '6a2751c7-012f-450a-920b-54a497627d87', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Winter hike in the woods_monikaUser.jpg'),
	(12, 'Green lantern', 'Just like in the DC Comics. The green lantern. He is real. Okay, jokes aside. He is very cute though.', '2023-05-03 19:37:58', 14, 12, 0, 0, 'f0b1b0ba-3394-44af-a344-27ee62f6fc90', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Green lantern_polyUser.jpg'),
	(13, 'Ink time', 'This photo is part of my friend Gamaliel\'s work day, the purpose of the photo is to highlight the hard work behind any great project.', '2023-05-03 19:38:17', 20, 42, 6, 3, 'ec75b3b3-70c1-402e-aff7-f14c45eef282', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /Ink time_gali.jpg'),
	(14, 'LEGENDARY IDOLS', 'The smell of books.', '2023-05-03 19:39:40', 21, 42, 0, 0, '243eece9-6599-4889-8ba9-007e9acf62d2', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Interior Design/LEGENDARY IDOLS_gali.jpg'),
	(15, 'My lovely cocker spaniel', 'This is my best friend. Her name is Kora. She is 6 years old and she loves having treats. Everyday. Every minute.', '2023-05-03 19:40:01', 17, 12, 10, 1, 'f83948c0-39b7-437a-8d22-aefd8c447284', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Dogs/My lovely cocker spaniel_polyUser.jpg'),
	(16, 'Windy day', 'During my summer camp, we had the pleasure to experience this breathtaking view. The whole group started taking photos, but I think that mine got all details right in place.', '2023-05-03 19:42:37', 19, 12, 4, 6, '1f112096-1d53-4c6f-a2bd-232218f48c2e', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Windy day_polyUser.jpg'),
	(17, 'Lonely chair', 'Lonely chair', '2023-05-03 19:42:52', 21, 40, 0, 0, 'f3982dc5-0c7e-4638-8fbf-3e56f0162d0a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Interior Design/Lonely chair_desi.jpg'),
	(18, 'The story melted my heart', 'We were on our medical mission trip in Sitio Linyama, Bongabong, Oriental Mindoro, Philippines when I saw this family arrive to the place where they dump all the bananas they have collected from the mountains. They sell these for P20.00 ($0.40) per bundle of bananas. They will use the money to buy rice to feed their stomachs for the day. What stirs me in this photo is how happy they still look after a long day of hard work. That just melts my heart.', '2023-05-03 19:44:37', 20, 40, 10, 1, '75a00ab1-c53b-4131-81b8-03530eef553a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /The story melted my heart_desi.jpg'),
	(19, 'Big flock of big birds', 'I was driving through my village when Ione of these guys passed above my car very fast. I stopped and i saw that he joined the others, so i decided to take the photo.', '2023-05-03 19:45:05', 14, 13, 0, 0, 'c800685e-cd06-411e-8377-1a7c015159e7', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Big flock of big birds_andrei.jpg'),
	(20, 'The great white bear', 'This is the most dangerous shot i\'ve ever taken. I hope you all like it.', '2023-05-03 19:46:33', 16, 13, 4.66667, 5, '2be57e59-3c75-44ed-ae1c-85914a5211e8', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sea Life/The great white bear_andrei.jpg'),
	(21, 'My best friend', 'Sometimes my friends tell me that i don\'t have a dog, but a wolf. I tell them that even the wolfs aren\'t that big. Take a look at Sara', '2023-05-03 19:48:06', 17, 13, 4.66667, 6, '5e509219-8cdf-49be-8e19-ce3bd2d4eb34', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Dogs/My best friend_andrei.jpg'),
	(22, 'My fluffy little baby', 'This is my lovely small ball of fur. He is so calm even though he is still a baby.', '2023-05-03 19:49:41', 18, 13, 9, 2, 'e7bdef17-d1a3-4527-8306-f9d4e20ff169', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Cats/My fluffy little baby_andrei.jpg'),
	(23, 'What a house', 'Greenhouse with colorful folding chair!', '2023-05-03 19:52:09', 21, 39, 0, 0, '1e0a8ecf-8f02-4528-a344-01e20d3a6980', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Interior Design/What a house_misho.jpg'),
	(24, 'Lake of fishermen', 'We were about to go fishing, but the landscape was amazing, so me and my friend made a bet who has the better photo of it. This is mine.', '2023-05-03 19:52:27', 19, 13, 9, 2, 'fe0232db-85e1-4b36-afcf-67db16662346', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Lake of fishermen_andrei.jpg'),
	(25, 'Octopus waves Hello', 'I was diving yesterday when i stumbled upon this little.Okay,maybe not little, but medium sized fella. He took off the second I tried to shoot him with my camera, but I still managed to capture him waving at me.', '2023-05-03 19:56:39', 16, 14, 10, 1, '496ae195-86e8-4640-b944-b44043d44dba', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sea Life/Octopus waves Hello_koko.jpg'),
	(26, 'Musician Busking', 'Anytime I’m in New Orleans my camera becomes glued to my hand - I absolutely love photographing the people there. From the food to the street performers, the city is filled with so much culture and history it makes every visit feel unique.', '2023-05-03 19:57:43', 20, 39, 9, 2, '71d339dc-24a1-4a7f-882a-8920ce1bd571', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /Musician Busking_misho.jpg'),
	(27, 'Alaska Big Boy', 'I am glad that im not the only one with this big guy around here. His name is Max and he weights almost the same as me. Bet he can pull a boat without a problem.', '2023-05-03 19:58:26', 17, 14, 9.33333, 2, '2048703a-37fd-49e1-a428-330bc80bccba', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Dogs/Alaska Big Boy_koko.jpg'),
	(28, 'Mountain work', 'Im glad that i work here everyday. It just makes me happy being around the nature. I know how energizing it can be even though we are exhausted from work.', '2023-05-03 20:00:54', 19, 14, 6.5, 5, 'd2ba0b53-2764-4ceb-872c-8466587bc233', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Mountain work_koko.jpg'),
	(29, 'Morning Scheduled Parrot Meet', 'I bet that they are talking about the climate change in the moment. Or maybe about the fourth guy who is late again. Casual workday in the office. Parrot office, of course.', '2023-05-03 20:03:25', 14, 15, 0, 0, '284f1b22-b7fd-4a22-9e3f-f8ecedd55575', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Morning Scheduled Parrot Meet_svetlio.jpg'),
	(30, 'Great White Encountered', 'I know that they are very dangerous and believe me I was frightened even though me and my friend were in the special cage.', '2023-05-03 20:04:53', 16, 15, 5.66667, 4, 'ec0ec77f-aa16-4d12-ac2b-0aba502b7fb1', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sea Life/Great White Encountered_svetlio.jpg'),
	(31, 'Lickable', 'That how my friend right here, named Maxi, likes to handle his hygiene after lunch. Of course, he is still hungry.', '2023-05-03 20:07:13', 17, 15, 7, 4, 'bd04c032-084d-4142-8abd-9544e3c39e22', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Dogs/Lickable_svetlio.jpg'),
	(32, 'Egyptian Cat', 'Hey,community this is Sandra. She is 4 years old and she likes to catch my laser. Today she was looking curious so I decided to take a photo of her. ', '2023-05-03 20:44:40', 18, 15, 5.66667, 4, '1c082e38-611e-4492-87fa-eea2e46e310e', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Cats/Egyptian Cat_svetlio.jpg'),
	(33, 'Hill of hills', 'This is mid October near Switzerland bounderies southwest. I don\'t know what is the name of this beautiful mount. But I think that the picture I took is worth it.', '2023-05-03 20:48:03', 19, 15, 7.5, 3, 'a47fd5bb-5aba-452f-ace6-40e121149787', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Hill of hills_svetlio.jpg'),
	(34, 'Colorful Parrot', 'This is Jack. He is one of the 5 Spanish speaking birds in my town. He can sing you a song if you give him a biscuit.', '2023-05-03 20:50:13', 14, 16, 0, 0, '833eb3c3-3744-4c6d-b627-d130b03b1312', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Colorful Parrot_vilko.jpg'),
	(35, 'New Aquarium Family Member', 'Hey, guys, i want to show you the newest species in my aquarium - the blue-bodied purple-tail fish. Sorry, I don\'t know whats the specie called.', '2023-05-03 20:52:42', 16, 16, 6, 3, '70705972-473c-4d14-ba76-2e32426eef66', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sea Life/New Aquarium Family Member_vilko.jpg'),
	(36, 'Cute puppy', 'Hey, community, take a look at Milo. He is the newest member of our family. We found him near the lake at mount Sky, he was alone and he needed water, so we decided to take him home. ', '2023-05-03 20:54:18', 17, 16, 9, 3, '3b65a6bd-27dc-41be-9814-4d91f53f7f84', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Dogs/Cute puppy_vilko.jpg'),
	(37, 'My Scotland Earlopped', 'Chester says hello to the Frame Frenzy community. Glad to be here.', '2023-05-03 20:55:52', 18, 16, 10, 1, '8c98b4ba-f232-4431-8176-9c1c7dccb00c', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Cats/My Scotland Earlopped_vilko.jpg'),
	(38, 'Silent Place', 'During my last hike i found myself in the middle of this breath-taking view. This place was so silent that it made me capture the moment, so you can hear it as I did.', '2023-05-03 20:57:33', 19, 16, 7, 4, 'fe8e9b70-daf0-48f9-89f3-796488c67df6', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Silent Place_vilko.jpg'),
	(39, 'Uninvited Guest', 'I hope that this guy has a reason to enter his property, otherwise they are in for a sing contest. So I caught everything in the moment. Check it out.', '2023-05-03 20:59:45', 14, 17, 0, 0, 'a284a6c8-26de-45ba-8d09-01c816c95478', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Birds/Uninvited Guest_mladen.jpg'),
	(40, 'Old and Wise Turtle', 'I was on a vacation with my wife, and one morning we woke up and this little fella was watching us like this. I took my camera and took my shot. Incredible.', '2023-05-03 21:01:38', 16, 17, 2.66667, 6, 'f7c015ae-8a6f-478f-9fcb-105eb54f8fd8', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sea Life/Old and Wise Turtle_mladen.jpg'),
	(41, 'Flying fluff', 'Hey, guys, yesterday i took my dog for a walk. Or should I say, for a flight.', '2023-05-03 21:03:20', 17, 17, 5.66667, 5, '24be8985-e864-47b6-b1ae-f6d03e49991a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Dogs/Flying fluff_mladen.jpg'),
	(42, 'Concentration', 'How concentrated do you think that he is? By the way, this is Richard, he wants the tuna that is in my hand. Thats why I took the photo. ', '2023-05-03 21:04:40', 18, 17, 6.66667, 3, '2f65c49a-bf6c-4772-a382-7bfb00f48b59', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Cats/Concentration_mladen.jpg'),
	(43, 'Beautiful Sunset', 'I dont think that there are words to define the beauty of mother nature.', '2023-05-03 21:05:46', 19, 17, 9, 2, '85b9bbf8-01ea-4f7a-ba5f-795f5e0423f6', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Majestic Landscapes/Beautiful Sunset_mladen.jpg'),
	(44, 'Franziskaner loaf', 'Here you can see the crispy, wonderful smelling Franziskaner-loaf and rye whole-grain tin loaf all baked by Franziskaner bakery in Bozen (Italy)', '2023-05-04 11:04:32', 20, 34, 0, 4, '6d7b0c84-91ce-4a49-ab98-465db12ecc4d', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/People at Work /Franziskaner loaf_misheto.jpg'),
	(45, 'Sweetness overloaded', 'Tray of colourful decorated cupcakes.', '2023-05-04 11:12:52', 23, 34, 0, 0, '8fc38704-0891-4a8d-82d0-4a4f56ab5731', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Desserts/Sweetness overloaded_misheto.jpg'),
	(46, 'Down The Street', 'Down The Street', '2023-05-04 11:26:49', 24, 34, 0, 0, '8e835753-36db-472d-b609-4ddac181b22d', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Lights at Night/Down The Street_misheto.jpg'),
	(47, 'Cozy Living Room', 'This is part of the apartment of my friend, which he designed by himself. I hope you\'ll like it.', '2023-05-04 12:24:07', 21, 51, 0, 0, '9a46027b-77fc-4cd9-b23c-2dfa033145e1', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Interior Design/Cozy Living Room_gagula.jpg'),
	(48, 'Red Roses', 'Girona’s old town overflows with colourful displays during the “Temps de Flors” event each spring. The streets of Girona fill with colour and scent as the city hosts its annual flower festival in the medieval quarter.', '2023-05-04 15:05:30', 25, 51, 10, 1, '2f63e6a1-d6d7-48c4-b2f5-9a33b4572d62', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Flowers/Red Roses_gagula.jpg'),
	(49, 'Muiderslot Castle', 'Historical building in Holland', '2023-05-04 15:09:12', 26, 51, 0, 0, '56d27c9a-28e4-4ccc-81aa-cce4c653a050', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Castles/Muiderslot Castle_gagula.jpg'),
	(50, 'Hopeful Horizons', 'After a long walk in the mountain, I finally saw the sunrise. It was worth it!', '2023-05-04 15:12:27', 27, 51, 0, 0, '7f0df358-6406-406d-82aa-a90d49d84235', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sunrises/Hopeful Horizons_gagula.jpg'),
	(51, 'Wild Beach', 'Beautiful wild beach in Thailand. ', '2023-05-04 15:16:35', 29, 51, 8, 2, 'd5999441-91d1-4f19-a304-fe94055a58ca', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Beaches/Wild Beach_gagula.jpg'),
	(52, 'Brownies', 'Coconut flour raspberry brownies.', '2023-05-04 15:20:52', 23, 53, 0, 0, '90e0a398-c567-47ef-9653-c1b0a465f93a', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Desserts/Brownies_padre.jpg'),
	(53, 'Country Wildlife', 'Today I found a fox near my house and decided to make a photo of it. I think it looks nice, what will you tell? ', '2023-05-04 15:23:28', 28, 53, 8.66667, 2, '4d8cb55e-930a-4085-aa54-69264165a763', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Wild Animals/Country Wildlife_padre.jpg'),
	(54, 'The best start of the day', 'The morning view near my village house. I\'m glad to have made the transition from city life.', '2023-05-04 15:27:00', 27, 53, 0, 0, 'b15f403e-f121-4e29-8431-e456774035ad', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sunrises/The best start of the day_padre.jpg'),
	(55, 'Backyard Flowers', 'Some of my backyard flowers. I can\'t stop looking at them.', '2023-05-04 15:30:58', 25, 53, 7, 3, '72e27eda-3987-4bfa-88e0-e9bbe6d6da11', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Flowers/Backyard Flowers_padre.jpg'),
	(56, 'Beautiful Imperfection', 'I made this photo during my visit to Panama City Beach. I don\'t know why, but I like it a lot. What do you think?', '2023-05-04 15:35:39', 29, 53, 6, 4, 'd9f20ce5-0733-44fb-90e2-f257990a3112', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Beaches/Beautiful Imperfection_padre.jpg'),
	(57, 'Spring Is Coming', 'Japanese cherry blossom', '2023-05-04 15:40:08', 25, 54, 7.66667, 2, 'b659ac1f-7fd1-47fc-9fef-9b88e26bdc30', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Flowers/Spring Is Coming_kevinG.jpg'),
	(58, 'Eltz Castle', 'Early morning autumn vibes at Eltz Castle', '2023-05-04 15:41:22', 26, 54, 0, 0, '7e176d4c-d187-40b5-86f2-2f1071df2081', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Castles/Eltz Castle_kevinG.jpg'),
	(59, 'Freedom', 'At 5.30am we stand at the top of the hill in the Mt Cook national park in New Zealand to celebrate that awesome sunrise. The photo was taken with the self-timer.', '2023-05-04 15:43:02', 27, 54, 0, 0, '4ee3c652-1871-4071-92e5-14ebf1415ee7', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Sunrises/Freedom_kevinG.jpg'),
	(60, 'Deep in the Forest', 'Kerala forest amazing where we could see beautiful deer and feeding time. Enjoyed the moment and more meaning full terms of nature.', '2023-05-04 15:45:07', 28, 54, 10, 1, '212cd364-9123-41c0-9362-5bbc3fbd7991', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Wild Animals/Deep in the Forest_kevinG.jpg'),
	(61, 'Venice Beach', 'This is my first photoshoot with my first professional camera. I love my job as a software developer but I just wasn’t born to be in an office from 9-5, specially living in California. So I decided to follow my two biggest passions photography and travel.', '2023-05-04 15:46:58', 29, 54, 9.66667, 1, '7d1b8269-54e2-4ef9-8d79-6b68d88e96ab', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Beaches/Venice Beach_kevinG.jpg'),
	(62, 'Beauty of the Beach', 'Beach of Byron Bay, friends and families enjoying the sun and the ocean.', '2023-05-04 15:51:46', 29, 55, 7.33333, 3, '8bb764ea-6196-44ed-aa46-d36346b5b1f6', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Beaches/Beauty of the Beach_dayana.jpg'),
	(64, 'Crocodylus Niloticus', 'A Nile Crocodile moving back into the Levuvuh River in the Kruger', '2023-05-04 16:05:50', 28, 55, 8, 3, '14a3077b-514a-4572-b32c-c09393c22629', 'https://photocontestteam14.s3.eu-north-1.amazonaws.com/contests/Wild Animals/Crocodylus Niloticus_dayana.jpg');
/*!40000 ALTER TABLE `participations` ENABLE KEYS */;

-- Dumping data for table photo-contest.ranks: ~4 rows (approximately)
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` (`rank_id`, `name`, `points`) VALUES
	(1, 'Junkie', 0),
	(2, 'Enthusiast', 51),
	(3, 'Master', 151),
	(4, 'Wise and Benevolent Photo Dictator', 1001);
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;

-- Dumping data for table photo-contest.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'Organizer'),
	(2, 'Photo Junkie');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table photo-contest.users: ~33 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`, `role_id`, `rank_id`, `points`, `profile_photo`, `uuid`, `is_enabled`, `is_locked`, `password_reset_token`, `email_confirmation_token`) VALUES
	(7, 'Ivan', 'Ivanov', 'ivanUser', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'ivan@company.com', 1, 3, 152, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '20b4af9a-f2fd-426f-8b4c-3ec175654a8e', 1, 0, NULL, NULL),
	(8, 'Petar', 'Petrov', 'pesho', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'pesho@company.com', 1, 3, 160, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '32b87044-5092-45e6-8203-7c84e948b190', 1, 0, NULL, NULL),
	(9, 'Georgi', 'Georgiev', 'goshoUser', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'georgi@company.com', 2, 1, 0, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'a6d93b75-0910-4094-a369-e4e427db7706', 1, 0, NULL, NULL),
	(10, 'Monika', 'Dimitrova', 'monikaUser', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy', 'monik@company.com', 2, 4, 1287, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '938fe895-6176-422b-b03d-b617cea6c4b3', 1, 0, NULL, NULL),
	(11, 'Vesela', 'Todorova', 'veselaUser', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi', 'vesela@company.com', 2, 3, 502, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'f4d73693-c560-4944-9a54-60c7be4ca4fc', 1, 0, NULL, NULL),
	(12, 'Paolina', 'Stoyanova', 'polyUser', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC', 'poly@company.com', 2, 3, 553, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '6074ddb5-effa-4a05-8714-5aec59725b14', 1, 0, NULL, NULL),
	(13, 'Andrei', 'Georgiev', 'andrei', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'andrei@company.com', 2, 2, 65, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '5f5eb266-273c-4629-8261-5a72f0611f34', 1, 0, NULL, NULL),
	(14, 'Kaloyan', 'Kirov', 'koko', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'koko@company.com', 2, 2, 89, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '56cd5eb1-777f-4355-ad4a-9f8902291a30', 1, 0, NULL, NULL),
	(15, 'Svetlozar', 'Ivanov', 'svetlio', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'svetlio@company.com', 2, 1, 25, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '9ca08bf1-3bf5-4ced-a82b-4328a6ca80cd', 1, 0, NULL, NULL),
	(16, 'Vilian', 'Terziev', 'vilko', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'vilko@company.com', 2, 2, 95, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '18227da0-fb8a-47a7-8cef-c277bd31b977', 1, 0, NULL, NULL),
	(17, 'Mladen', 'Georgiev', 'mladen', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'mladen@company.com', 2, 1, 50, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '6081895c-b1b1-4d0c-9755-96ad3f7768c0', 1, 0, NULL, NULL),
	(34, 'Daniela', 'Mihaylova', 'misheto', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'mishe@company.com', 2, 1, 34, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '7161048b-e0e1-4fa5-a238-dd8d2958768c', 1, 0, NULL, NULL),
	(35, 'Zdravko', 'Blagoev', 'zdravec', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'zdravko@company.com', 2, 3, 231, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'e61326ac-8a40-4f66-b5ea-a24023941292', 1, 0, NULL, NULL),
	(36, 'Velislava', 'Borisova', 'veliUser', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'veli@company.com', 2, 1, 3, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'da4d34a3-2cbd-4be0-8f9d-55c288a2ee96', 1, 0, NULL, NULL),
	(37, 'Stanislav', 'Velkov', 'staniUser', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy', 'stani@company.com', 2, 2, 70, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '8a0e90d3-2bdc-47c9-a494-851dda1a70f7', 1, 0, NULL, NULL),
	(38, 'Bilyana', 'Hristova', 'bibi', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi', 'bibi@company.com', 1, 3, 666, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'ef7c94d3-568a-4f0f-9839-fdd9f89efaf2', 1, 0, NULL, NULL),
	(39, 'Mihail', 'Nikolaev', 'misho', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC', 'misho@company.com', 2, 2, 57, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '054cec93-cdd9-4556-a343-f22b91dfef11', 1, 0, NULL, NULL),
	(40, 'Desislava', 'Georgieva', 'desi', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'desi@company.com', 2, 1, 2, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '53891ce4-edde-467f-8ec4-7e974e3e53b9', 1, 0, NULL, NULL),
	(41, 'Simeon', 'Iliev', 'simo', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'simo@company.com', 2, 1, 0, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '4e01c578-4ae9-4494-97d3-4f9527d36124', 1, 0, NULL, NULL),
	(42, 'Galina', 'Stanimirova', 'gali', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'gali@company.com', 2, 1, 2, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'ca94886f-3a0a-4ea3-8161-850e108a5986', 1, 0, NULL, NULL),
	(43, 'Dimitar', 'Dimitrov', 'mitko', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'mitko@company.com', 1, 4, 1993, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'e1089d42-ac5b-4271-bfc9-8849488f9430', 1, 0, NULL, NULL),
	(44, 'Metodi', 'Avramov', 'meto', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'meto@company.com', 2, 1, 2, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'd6a12854-9c25-41ab-b71a-50be5c81e8a6', 1, 0, NULL, NULL),
	(49, 'Panayot', 'Zarov', 'pongo', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'pongo@company.com', 1, 3, 157, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '25247825-e612-4c0a-b324-912e3a49bd9b', 1, 0, NULL, NULL),
	(50, 'Anelia', 'Ivanova', 'anche', '$2a$10$nZrzkB7SHIu27P3YQ4koJ.CzxW.A/0JLotbYOarNPQlzeWOOGVctG', 'anche@company.com', 1, 3, 160, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'fef86524-6ba5-4e8d-bb5e-81c1356fdd62', 1, 0, NULL, NULL),
	(51, 'Atanas', 'Gagulov', 'gagula', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'gagula@company.com', 2, 1, 5, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '459899d0-4387-4869-989f-45edb72c4c7f', 1, 0, NULL, NULL),
	(52, 'Nikolai', 'Ivanov', 'nikiSun', '$2a$10$LTC6RrueeZhy6N/UIeD4I..xGqoipole/WIjfaZDDV4LDz.502ruy', 'nikiSun@company.com', 2, 4, 1200, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'c40ae58d-f998-440c-9ec8-8acf57639847', 1, 0, NULL, NULL),
	(53, 'Petar', 'Ivanov', 'padre', '$2a$10$ppIYNfl0TF8nuphXSp2VJ.vu6SgHVqqAS1HxFezFPcW9eDvbhSqBi', 'padre@company.com', 2, 3, 505, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'c3177f7a-1010-4d85-b427-2040b4eb43bb', 1, 0, NULL, NULL),
	(54, 'Nikolai', 'Simeonov', 'kevinG', '$2a$10$jTby6OE7JiPF..Fqx55wIOfaVef/DtnqFdSfapL7CzC4zONmuKDwC', 'kevinG@company.com', 2, 3, 505, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', 'eae5869f-fd96-462a-88d6-31d48775d9d9', 1, 0, NULL, NULL),
	(55, 'Dayana', 'Kyoseva', 'dayana', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'dayana@company.com', 2, 1, 3, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '3e044ec5-d01d-4d9e-8d91-ff37b44cbe40', 1, 0, NULL, NULL),
	(56, 'Galin', 'Shterev', 'galcho', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'galcho@company.com', 2, 1, 0, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '8f15743c-460d-4a76-9a6d-95a4badab193', 1, 0, NULL, NULL),
	(57, 'Simeon', 'Radev', 'monchi', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'monchi@company.com', 2, 1, 0, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '45113b0f-9ead-456b-b6fb-6421a2bbdba7', 1, 0, NULL, NULL),
	(58, 'Mikaela', 'Stamenova', 'mika', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'mika@company.com', 2, 1, 0, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '406e2364-4071-4f76-aec6-2f560f876638', 1, 0, NULL, NULL),
	(59, 'Plamen', 'Kyosev', 'plamkata', '$2a$10$UB.n5N/J5SexQFASqURF1.4x5Y3ADKJiWQfYLgcIkENkujKsO5cX6', 'plamkata@company.com', 2, 1, 0, 'https://img.freepik.com/premium-vector/holding-camera-with-two-hands-illustration-cartoon-vector_484148-252.jpg?w=740', '121af13b-fd6a-495d-a122-34cdb00b81c5', 1, 0, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
